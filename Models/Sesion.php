<?php

class Sesion{
	public $id;
	public $nombre;
	public $descripcion;
	public $lugar;
	public $fechaHora;	
	public $duracion;	
	public $idActividad;

	function Sesion($id, $mysqli){
		$peticion = $mysqli->query('SELECT * FROM `sesiones` WHERE id = '.$id.' LIMIT 1');
		if($peticion){
			$fila = $peticion->fetch_assoc();
			$this->id = $fila['id'];
			$this->nombre = $fila['nombre'];			
			$this->descripcion = $fila['descripcion'];	
			$this->lugar = $fila['lugar'];	
			$this->fechaHora = $fila['fechaHora'];	
			$this->duracion = $fila['duracion'];	
			$this->idActividad = $fila['id_actividad'];									
			return $this;
		}else{
			return NULL;
		}
	}

}		

?>	