<?php

class Actividad{
	public $id;
	public $nombre;	
	public $tipo;
	public $objetivo;
	public $resultado;		
	public $volumen;	
	public $convocatoria;	
	public $responsable;
	public $activo;
	//public $actividades;
	public $colaboradores;
	public $beneficiarios;
	public $sesiones;


	function Actividad($id, $mysqli){
		$peticion = $mysqli->query('SELECT * FROM `actividades` WHERE id = '.$id.' LIMIT 1');
		if($peticion->num_rows > 0){
			$fila = $peticion->fetch_assoc();
			$listC = array();
			$listB = array();
			$listA = array();
			$listS = array();
			
			$queryC = $mysqli->query('SELECT colaboradores.id FROM actividades, actividades_colaboradores, colaboradores WHERE actividades.id = actividades_colaboradores.id_actividades AND colaboradores.id = actividades_colaboradores.id_colaboradores AND actividades.id = '.$fila['id']);
			if($queryC->num_rows != 0){
				$listC = array();
				$contC = 0;
				for($num = $queryC->num_rows - 1; $num>=0;$num--){	
					$filaC = $queryC->fetch_assoc();
					$itemC = $filaC['id']; #new Colaborador( $filaC['id'] ,$mysqli);
					$listC[$contC] = $itemC;
					$contC++;
				}
			}

			$queryB = $mysqli->query('SELECT beneficiarios.id FROM actividades, activiadades_beneficiarios, beneficiarios WHERE actividades.id = activiadades_beneficiarios.id_actividades AND beneficiarios.id = activiadades_beneficiarios.id_beneficiarios AND actividades.id = '.$fila['id']);
			if($queryB->num_rows != 0){
				$listB = array();
				$contB = 0;
				for($num = $queryB->num_rows - 1; $num>=0;$num--){	
					$filaB = $queryB->fetch_assoc();
					$itemB = $filaB['id']; #new Beneficiario( $filaB['id'] ,$mysqli);
					$listB[$contB] = $itemB;
					$contB++;
				}
			}

			/*$queryA = $mysqli->query('SELECT actividades.id FROM `actividades` WHERE actividades.id_actividad = actividades.id AND actividades.id = '.$fila['id']);
			if($queryA->num_rows != 0){
				$listA = array();
				$contA = 0;
				for($num = $queryA->num_rows - 1; $num>=0;$num--){	
					$filaA = $queryA->fetch_assoc();
					$itemA = $filaA['id']; #new Actividad( $filaA['id'] ,$mysqli);
					$listA[$contA] = $itemA;
					$contA++;
				}
			}	*/	

			$queryS = $mysqli->query('SELECT sesiones.id FROM actividades, sesiones WHERE actividades.id = sesiones.id_actividad AND actividades.id = '.$fila['id'].' ORDER BY sesiones.fechaHora ');
			if($queryS->num_rows != 0){
				$listS = array();
				$contS = 0;
				for($num = $queryS->num_rows - 1; $num>=0;$num--){	
					$filaS = $queryS->fetch_assoc();
					$itemS = $filaS['id']; #new Sesion( $filaS['id'] ,$mysqli);
					$listS[$contS] = $itemS;
					$contS++;
				}
			}		
			
			$this->id = $fila['id'];
			$this->nombre = $fila['nombre'];
			$this->tipo = $fila['tipo'];
			$this->objetivo = $fila['objetivo'];
			$this->resultado = $fila['resultado'];						
			$this->volumen = $fila['volumen'];			
			$this->convocatoria = $fila['convocatoria'];
			$this->responsable = $fila['id_colaborador'];
			$this->activo = $fila['activo'];
			$this->colaboradores = $listC;
			$this->beneficiarios = $listB;
			//$this->actividades = $listA;
			$this->sesiones = $listS;
			
			return $this;
		}else{
			return $this;
		}
	}

}		

?>	