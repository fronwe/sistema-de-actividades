<?php
session_start();

header('Content-Type: text/html; charset=ISO-8859-1');

include 'HomeController.php';
include 'ColaboradorController.php';
include 'ActividadController.php';
include 'SesionController.php';
include 'BeneficiarioController.php';
include 'UnidadAcademicaController.php';

$HomeController = new HomeController();
$ColaboradorController = new ColaboradorController();
$ActividadController = new ActividadController();
$SesionController = new SesionController();
$BeneficiarioController = new BeneficiarioController();
$UnidadAcademicaController = new UnidadAcademicaController();

$mysqli = $HomeController->conect();

$_FUNCION = '';
if(isset($_GET['funcion'])){
	$_FUNCION = $_GET['funcion'];
}

if($_FUNCION == 'login'){
	
	if( isset($_POST['usuario']) && isset($_POST['clave']) ){
		
		$email = $_POST['usuario'];
		$clave = $_POST['clave'];

		$colaboradores = $ColaboradorController->colaboradores($mysqli);
		
		$colaborador = null;
		foreach ($colaboradores as $key => $value) {
			if( $value->clave == $clave && $value->email == $email && $value->activo == 1 ){
				if( $value->permiso == 'Administrador' || $value->permiso == 'Responsable'){
					$colaborador = $value;
					$_SESSION['colaborador'] = json_encode($colaborador);
					if(md5($colaborador->identificacion) == $colaborador->clave){
						echo '2';
					}else{
						echo '1';
					}
				}
			}
		}
		
		if( $colaborador == null){
			echo '0';			
		}		
	}	
}

if($_FUNCION == 'reasignar'){
	
	if( isset($_POST['usuario']) && isset($_POST['identificacion']) && isset($_POST['clave']) ){
		
		$email = $_POST['usuario'];
		$identificacion = $_POST['identificacion'];
		$clave = $_POST['clave'];

		$colaboradores = $ColaboradorController->colaboradores($mysqli);
		
		$colaborador = null;
		foreach ($colaboradores as $key => $value) {
			if( $value->identificacion == $identificacion && $value->email == $email){
				if( $value->permiso == 'Responsable'){
					$colaborador = $value;					
					if( md5($colaborador->identificacion) == $colaborador->clave){
						$reasingacion = $ColaboradorController->resignarClave($identificacion, $email, $clave, $mysqli);
						if($reasingacion == 1){
							echo 1;
							$_SESSION['colaborador'] = json_encode($colaborador);
						}					
					}else{	
						echo 0;	
					}					
				}
			}
		}
		
		if( $colaborador == null){
			echo '0';			
		}		
	}	
}


if($_FUNCION == 'restaurarClaveColaborador'){	
	if( isset($_POST['id']) &&  isset($_POST['identificacion'])  ){
		$idColaborador = $_POST['id'];
		$identificacion = $_POST['identificacion'];
		echo $ColaboradorController->restaurarClaveColaborador($idColaborador, $identificacion,$mysqli);		
	}
}




if($_FUNCION == 'logout'){	
	unset($_SESSION['colaborador']);	
}

if($_FUNCION == 'crearActividad'){
	if( isset($_POST['json']) ){
		$obj = json_decode($_POST['json'], true);		
		$valid = 1;		

		$idActivdad = $ActividadController->crearActividad($obj, $mysqli);
		if($idActivdad != 0){

			for ($i=0; $i < count($obj['sesiones']); $i++) { 
				$valid = $SesionController->crearSession($obj['sesiones'][$i], $idActivdad, $mysqli);

			}			

			if($obj['convocatoria'] == 'Especifica'){
				for ($i=0; $i < count($obj['beneficiarios']); $i++) { 
					$BeneficiarioController->asignarBeneficiarios( $idActivdad, $obj['beneficiarios'][$i], $mysqli );			
				}
			}

			for ($i=0; $i < count($obj['colaboradores']); $i++) { 
				$valid = $ColaboradorController->asignarColaboradores( $idActivdad, $obj['colaboradores'][$i], $mysqli );							
			}
			
			if ($valid != 0){

				echo $idActivdad;	
			}else{
				echo 0;
			}		
		}else{
			echo 0;
		}
	}	
}


if($_FUNCION == 'editarActividad'){
	if( isset($_POST['json']) ){
		$obj = json_decode($_POST['json'], true);		
		$valid = 1;		
	
		$delete = $ActividadController->eliminarActividad($obj['id'], $mysqli);

		if($delete != 0){

			$idActivdad = $ActividadController->crearActividad($obj, $mysqli);	
			if($idActivdad != 0){

				for ($i=0; $i < count($obj['sesiones']); $i++) { 
					$valid = $SesionController->crearSession($obj['sesiones'][$i], $idActivdad, $mysqli);
				}			

				if($obj['convocatoria'] == 'Especifica'){
					for ($i=0; $i < count($obj['beneficiarios']); $i++) { 
						$BeneficiarioController->asignarBeneficiarios( $idActivdad, $obj['beneficiarios'][$i], $mysqli );			
					}				
				}

				for ($i=0; $i < count($obj['colaboradores']); $i++) { 
					$valid = $ColaboradorController->asignarColaboradores( $idActivdad, $obj['colaboradores'][$i], $mysqli );							
				}

				if ($valid != 0){
					echo $idActivdad;	
				}else{
					echo 0;
				}		
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}

	}	
}


if($_FUNCION == 'tiposColaborador'){
	$tipos = $ColaboradorController->tiposColaborador($mysqli);
	if($tipos != 0){
		echo  json_encode($tipos);	
	}else{
		echo 0;
	}
	
}


if($_FUNCION == 'areasAcademicasTipo'){
	if( isset($_POST['tipo']) ){
		$tipo =  $_POST['tipo'];
		$areas = $ColaboradorController->areasAcademicasTipo($tipo, $mysqli);
		if($areas != 0){
			echo json_encode($areas);			
		}else{
			echo 0;
		}
		
	}
}


if($_FUNCION == 'colaboradoresArea'){
	if( isset($_POST['idArea']) ){
		$tipo =  $_POST['tipo'];
		$idArea =  $_POST['idArea'];

		$colaboradores = $ColaboradorController->colaboradoresArea($tipo, $idArea, $mysqli);
		if($colaboradores != 0){
			echo json_encode($colaboradores);	
		}else{
			echo 0;
		}		
	}
}


if($_FUNCION == 'verActividad'){
	if( isset($_POST['id']) ){
		$idActividad = $_POST['id'];
		$actividad = $ActividadController->actividad($idActividad, $mysqli);

		$idResponsable = $actividad->responsable;
		$actividad->responsable = $ColaboradorController->colaborador($idResponsable, $mysqli);

		$idUnidadAcademica = $actividad->responsable->unidadAcademica;
		$actividad->responsable->unidadAcademica = $UnidadAcademicaController->UnidadAcademica($idUnidadAcademica, $mysqli);

		foreach ($actividad->beneficiarios as $key => $value) {
			$actividad->beneficiarios[$key] = $BeneficiarioController->beneficiario($value, $mysqli);
		}
	
		foreach ($actividad->colaboradores as $key => $value) {

			$colaborador = $ColaboradorController->colaborador($value, $mysqli);
			$idUnidad = $colaborador->unidadAcademica;
			$colaborador->unidadAcademica = $UnidadAcademicaController->UnidadAcademica($idUnidad, $mysqli);
			$actividad->colaboradores[$key] = $colaborador;

		}

		foreach ($actividad->sesiones as $key => $value) {
			$actividad->sesiones[$key] = $SesionController->sesion($value, $mysqli);
		}				
		
		echo json_encode($actividad);	
	}
}


if($_FUNCION == 'actividadesColaboradorCreador'){
	if( isset($_POST['id']) ){
		$idColaborador = $_POST['id'];
			
		$list = array();
		
		$listId = $ActividadController->actividadesColaboradorCreador($idColaborador, $mysqli);
				
		if($listId != 0){
			$count = count($listId);
			foreach ($listId as $indice => $valor) {				
			
				$actividad = $ActividadController->actividad($valor, $mysqli);
				$idResponsable = $actividad->responsable;
				$actividad->responsable = $ColaboradorController->colaborador($idResponsable, $mysqli);

				$idUnidadAcademica = $actividad->responsable->unidadAcademica;
				$actividad->responsable->unidadAcademica = $UnidadAcademicaController->UnidadAcademica($idUnidadAcademica, $mysqli);

				foreach ($actividad->beneficiarios as $key => $value) {
					$actividad->beneficiarios[$key] = $BeneficiarioController->beneficiario($value, $mysqli);
				}

				foreach ($actividad->colaboradores as $key => $value) {
					$colaborador = $ColaboradorController->colaborador($value, $mysqli);
					$idUnidad = $colaborador->unidadAcademica;
					$colaborador->unidadAcademica = $UnidadAcademicaController->UnidadAcademica($idUnidad, $mysqli);
					$actividad->colaboradores[$key] = $colaborador;
				}

				foreach ($actividad->sesiones as $key => $value) {
					$actividad->sesiones[$key] = $SesionController->sesion($value, $mysqli);
				}

				$list[$indice] = $actividad;
				
			}			
		
			echo json_encode($list);		
			
		}else{
			echo 0;
		}	
	}
}

if($_FUNCION == 'actividadesColaboradorAsignado'){
	if( isset($_POST['id']) ){
		$idColaborador = $_POST['id'];

		$lista = array();

		$listId = $ActividadController->actividadesColaboradorAsignado($idColaborador, $mysqli);
		if($listId != 0){
			
			foreach ($listId as $indice => $valor) {				
			
				$actividad = $ActividadController->actividad($valor, $mysqli);

				$idResponsable = $actividad->responsable;
				$actividad->responsable = $ColaboradorController->colaborador($idResponsable, $mysqli);

				$idUnidadAcademica = $actividad->responsable->unidadAcademica;
				$actividad->responsable->unidadAcademica = $UnidadAcademicaController->UnidadAcademica($idUnidadAcademica, $mysqli);

				foreach ($actividad->beneficiarios as $key => $value) {
					$actividad->beneficiarios[$key] = $BeneficiarioController->beneficiario($value, $mysqli);
				}

				foreach ($actividad->colaboradores as $key => $value) {
					$colaborador = $ColaboradorController->colaborador($value, $mysqli);
					$idUnidad = $colaborador->unidadAcademica;
					$colaborador->unidadAcademica = $UnidadAcademicaController->UnidadAcademica($idUnidad, $mysqli);
					$actividad->colaboradores[$key] = $colaborador;
				}

				foreach ($actividad->sesiones as $key => $value) {
					$actividad->sesiones[$key] = $SesionController->sesion($value, $mysqli);
				}

				$lista[$indice] = $actividad;
			}
			echo json_encode($lista);	
		}else{
			echo 0;
		}	
	}
}

if($_FUNCION == 'actividadesAdministrador'){
	if( isset($_POST['id']) ){
		$idColaborador = $_POST['id'];

		$lista = array();

		$listId = $ActividadController->actividadesAdministrador($mysqli);
		if($listId != 0){
			
			foreach ($listId as $indice => $valor) {				
			
				$actividad = $ActividadController->actividad($valor, $mysqli);

				$idResponsable = $actividad->responsable;
				$actividad->responsable = $ColaboradorController->colaborador($idResponsable, $mysqli);

				$idUnidadAcademica = $actividad->responsable->unidadAcademica;
				$actividad->responsable->unidadAcademica = $UnidadAcademicaController->UnidadAcademica($idUnidadAcademica, $mysqli);

				foreach ($actividad->beneficiarios as $key => $value) {
					$actividad->beneficiarios[$key] = $BeneficiarioController->beneficiario($value, $mysqli);
				}

				foreach ($actividad->colaboradores as $key => $value) {
					$colaborador = $ColaboradorController->colaborador($value, $mysqli);
					$idUnidad = $colaborador->unidadAcademica;
					$colaborador->unidadAcademica = $UnidadAcademicaController->UnidadAcademica($idUnidad, $mysqli);
					$actividad->colaboradores[$key] = $colaborador;
				}

				foreach ($actividad->sesiones as $key => $value) {
					$actividad->sesiones[$key] = $SesionController->sesion($value, $mysqli);
				}

				$lista[$indice] = $actividad;
			}
			echo json_encode($lista);	
		}else{
			echo 0;
		}	
	}
}

if($_FUNCION == 'eliminarActividad'){	
	if( isset($_POST['id']) ){
		$idActividad = $_POST['id'];
		echo $ActividadController->eliminarActividad($idActividad, $mysqli);		
	}
}

if($_FUNCION == 'unidadesAcademicas'){	
	$lista = $UnidadAcademicaController->sesiones($mysqli);
	if($lista != NULL){
		echo json_encode($lista);			
	}else{
		echo 0;
	}
}




if($_FUNCION == 'crearColaborador'){	
	if( isset($_POST['colaborador']) ){
		$colaborador = json_decode($_POST['colaborador']);
		echo $ColaboradorController->crearColaborador($colaborador, $mysqli);
	}
}

if($_FUNCION == 'editarColaborador'){	
	if( isset($_POST['colaborador']) ){
		$colaborador = json_decode($_POST['colaborador']);		
		echo $ColaboradorController->editarColaborador($colaborador, $mysqli);
	}
}


if($_FUNCION == 'colaboradores'){	
	$lista = $ColaboradorController->responsables($mysqli);
	if($lista != NULL){		
		foreach ($lista as $key => $value) {		
			$idUnidad = $value->unidadAcademica;
			$value->unidadAcademica = $UnidadAcademicaController->UnidadAcademica($idUnidad, $mysqli);		
		}

		if($lista != NULL){
			echo json_encode($lista);			
		}else{
			echo 0;
		}
	}else{
		echo 0;
	}
}


if($_FUNCION == 'eliminarColaborador'){	
	if( isset($_POST['id']) ){
		$idColaborador = $_POST['id'];
		echo $ColaboradorController->eliminarColaborador($idColaborador, $mysqli);		
	}
}


//=========================================================================================================================================================//
//==================================================================NUEVO DESARROLLO=======================================================================//
//=========================================================================================================================================================//


if($_FUNCION == 'unidadesAcademicasTipo'){	
	if( isset($_POST['tipo'])){
		$tipo = $_POST['tipo'];			
		$lista = $UnidadAcademicaController->unidadesAcademicasTipo($mysqli, $tipo);							
		if($lista != NULL){
			echo json_encode($lista);
		}else{
			echo 0;
		}			
		
	}	
}

if($_FUNCION == 'consultarColaboador'){	
	if( isset($_POST['tipoIdent']) &&  isset($_POST['identificacion'])){
		$tipoIdent = $_POST['tipoIdent'];
		$identificacion = $_POST['identificacion'];
		$permiso = $_POST['permiso'];
		$colaborador = $ColaboradorController->consultarColaboador($mysqli, $tipoIdent, $identificacion, $permiso);
		if($colaborador != NULL){
			echo json_encode($colaborador);			
		}else{
			echo 0;
		}	
	}	
}

if($_FUNCION == 'desactivarActivades'){	
	echo $ActividadController->desactivarActivades($mysqli);			
}



if($_FUNCION == 'consultarBeneficiario'){	
	if( isset($_POST['tipoIdent']) && isset($_POST['identificacion']) ){
		$tipoIdent = $_POST['tipoIdent'];
		$identificacion = $_POST['identificacion'];		
		$beneficiario = $BeneficiarioController->consultarBeneficiario($mysqli, $identificacion, $tipoIdent);
		if($beneficiario != NULL){
			echo json_encode($beneficiario);			
		}else{
			echo 0;
		}	
	}	
}

$mysqli->close();

?>
