<?php

//require_once 'C:/xampp/htdocs/PSU/Models/Beneficiario.php';
require_once '../Models/Beneficiario.php';

class BeneficiarioController{

	public function beneficiario($id, $mysqli){
		$obj = new Beneficiario($id, $mysqli);
		return $obj;
	}

	public function beneficiarios($mysqli){
		$lista = array();
		$cont = 0;
		$query = 'SELECT id FROM actividades WHERE 1 ';		
		$peticion = $mysqli->query($query);
		if($peticion->num_rows > 0){
			for($num = $peticion->num_rows - 1; $num>=0;$num--){
				$fila = $peticion->fetch_assoc();
				$obj = new Beneficiario($fila['id'], $mysqli);
				$lista[$cont] = $obj;
				$cont++;
			}
			return $lista;
		}else{
			return NULL;
		}
	}

	public function asignarBeneficiarios($idActividad , $beneficiario, $mysqli){			
		$idBeneficiario = $this->buscarBeneficiario($beneficiario['tipoIdentificacion'], $beneficiario['identificacion'],  $mysqli);		
		if($idBeneficiario == 0){
			$queryBenef = "INSERT INTO `beneficiarios` (`id`, `tipo_identificacion`, `identificacion`, `nombre`, `apellido`, `edad`, `sexo`) VALUES 
			(NULL, '".$beneficiario['tipoIdentificacion']."', '".$beneficiario['identificacion']."', '".$beneficiario['nombre']."', '".$beneficiario['apellido']."', '".$beneficiario['edad']."', '".$beneficiario['sexo']."');";
			$peticionBenef = $mysqli->query($queryBenef);
			if($peticionBenef){
				$queryIdBenef = "SELECT id FROM beneficiarios WHERE 1 ORDER BY id DESC LIMIT 1";
				$peticionIdBenef = $mysqli->query($queryIdBenef);
				if($peticionIdBenef){
					$filaIdBenef = $peticionIdBenef->fetch_assoc();	
					$idBenef = $filaIdBenef['id'];
					return $this->createActividadesBeneficiarios($idActividad, $idBenef, $mysqli);
				}				
			}else{
				return 0;
			}
		}else{
			return $this->createActividadesBeneficiarios($idActividad, $idBeneficiario, $mysqli);
		}
	}		

	public function createActividadesBeneficiarios($idActividad, $idBeneficiario, $mysqli){
		$queryAsingbenef = "INSERT INTO `activiadades_beneficiarios` (`id_actividades`, `id_beneficiarios`) VALUES ('".$idActividad."', '".$idBeneficiario."');";
		$peticionAsingbenef = $mysqli->query($queryAsingbenef);
		
		if($peticionAsingbenef){
			return 1;
		}else{
			return 0;
		} 
	}

	public function buscarBeneficiario($tipoIdent , $identificacion, $mysqli){
		$query = "SELECT id FROM `beneficiarios` WHERE tipo_identificacion = '".$tipoIdent."' AND identificacion  = '".$identificacion."'";		
		$peticion = $mysqli->query($query);		
		if($peticion && $peticion->num_rows > 0 ){
			$fila = $peticion->fetch_assoc();		
			return $fila['id'];
		}else{
			return 0;
		}
	}


	public function consultarBeneficiario($mysqli, $identificacion, $tipoIdent){
		$colaborador;	
		$query = 'SELECT id FROM beneficiarios WHERE tipo_identificacion = "'.$tipoIdent.'" AND identificacion = "'.$identificacion.'" ';				
		$peticion = $mysqli->query($query);
		if($peticion->num_rows > 0){
			$fila = $peticion->fetch_assoc();
			$colaborador = new Beneficiario($fila['id'], $mysqli);
			return $colaborador;
		}else{
			return NULL;
		}
	}	

}

?>
