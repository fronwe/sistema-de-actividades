<?php

//require_once 'C:/xampp/htdocs/PSU/Models/Actividad.php';
require_once '../Models/Actividad.php';

class ActividadController{

	public function actividad($id, $mysqli){
		$obj = new Actividad($id, $mysqli);		
		return $obj;
	}

	public function actividades($mysqli){
		$lista = array();
		$cont = 0;
		$query = 'SELECT id FROM actividades WHERE 1 ';		
		$peticion = $mysqli->query($query);
		if($peticion->num_rows > 0){
			for($num = $peticion->num_rows - 1; $num>=0;$num--){
				$fila = $peticion->fetch_assoc();
				$obj = new Actividad($fila['id'], $mysqli);
				$lista[$cont] = $obj;
				$cont++;
			}
			return $lista;
		}else{
			return NULL;
		}
	}

	public function crearActividad($obj, $mysqli){		
		$resultado = 'NULL';
		if( $obj['resultado'] != ''){
			$resultado = $obj['resultado'];
		}
		$query = "INSERT INTO `actividades` (`id`, `nombre`, `objetivo`, `resultado`, `volumen`, `convocatoria`, `id_actividad`, `id_colaborador`, `id_creador`, `tipo`, `activo`) VALUES (NULL, '".$obj['nombre']."', '".$obj['objetivo']."', '".$resultado."', '".$obj['volumen']."', '".$obj['convocatoria']."', NULL , '".$obj['idResponsable']."', '".$obj['creador']."', '".$obj['tipo']."', 1)";		
		$peticion = $mysqli->query($query);
		if($peticion){
			$queryId = 'SELECT id FROM `actividades` WHERE 1 ORDER BY id DESC LIMIT 1';
			$peticionId = $mysqli->query($queryId);
			if($peticionId){
				$filaId = $peticionId->fetch_assoc();
				return $filaId['id'];				
			}else{
				return 0;
			}			
		}else{
			return 0;
		}
	}

	public function actividadesColaboradorCreador($idColaborador, $mysqli){
		$lista = array();
		$cont = 0;
		$query = 'SELECT actividades.id AS idActividad FROM actividades, colaboradores WHERE colaboradores.id = actividades.id_colaborador AND actividades.id_colaborador = '.$idColaborador.' AND actividades.activo = 1';		
		$peticion = $mysqli->query($query);
		if($peticion->num_rows > 0){
			for($num = $peticion->num_rows - 1; $num>=0;$num--){
				$fila = $peticion->fetch_assoc();				
				$lista[$cont] = $fila['idActividad'];
				$cont++;
			}
			return $lista;
		}else{
			return 0;
		}
	}

	public function actividadesColaboradorAsignado($idColaborador, $mysqli){
		$lista = array();
		$cont = 0;
		$query = 'SELECT actividades.id AS idActivdades FROM actividades, colaboradores, actividades_colaboradores WHERE actividades.id = actividades_colaboradores.id_actividades AND colaboradores.id = actividades_colaboradores.id_colaboradores AND colaboradores.id = '.$idColaborador.' AND activo = 1';
	
		$peticion = $mysqli->query($query);
		if($peticion->num_rows > 0){
			for($num = $peticion->num_rows - 1; $num>=0;$num--){
				$fila = $peticion->fetch_assoc();				
				$lista[$cont] = $fila['idActivdades'];
				$cont++;  
			}
			return $lista;
		}else{
			return 0;
		}
	}


	public function actividadesAdministrador($mysqli){
		$lista = array();
		$cont = 0;
		$query = 'SELECT id FROM actividades WHERE activo = 1';
	
		$peticion = $mysqli->query($query);
		if($peticion->num_rows > 0){
			for($num = $peticion->num_rows - 1; $num>=0;$num--){
				$fila = $peticion->fetch_assoc();				
				$lista[$cont] = $fila['id'];
				$cont++;  
			}
			return $lista;
		}else{
			return 0;
		}
	}	


	public function eliminarActividad($idActividad ,$mysqli){
		$query = 'DELETE FROM `actividades` WHERE id = '.$idActividad;		
		$peticion = $mysqli->query($query);
		if($peticion){
			return 1;
		}else{
			return 0;
		}
	}

	public function desactivarActivades($mysqli){
		$query = "UPDATE actividades, sesiones SET activo = 0 WHERE actividades.id = sesiones.id_actividad AND sesiones.fechaHora > '2017-06-01' AND sesiones.fechaHora < '2017-12-15'";
		$peticion = $mysqli->query($query);
		if($peticion){
			return 1;
		}else{
			return 0;
		}
	}

}

?>
