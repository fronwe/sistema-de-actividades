$(document).ready(function(){    
  $.post(
    "Controllers/AjaxController.php?funcion=verActividad",            
    {
        id : idActividad                
    },
    function(data,status){       
  		var obj = $.parseJSON(data);
      $('#nombre').text(obj.nombre);      
      $('#volumen').text(obj.volumen);
      $('#tipo').text(obj.tipo);
      $('#objetivo').text(obj.objetivo);
      $('#responsable').text(obj.responsable.nombre + ' ' + obj.responsable.apellido +' - '+ obj.responsable.unidadAcademica.nombre);      
      $('#convocatoria').text(obj.convocatoria);      
      var resultado;
      if(obj.resultado == 'NULL'){
        resultado = 'Pendiente';
      }else{
        resultado = obj.resultado;
      }
      $('#resultado').append(resultado);

      var parid;
      var count = 0;
      for (i in obj.sesiones ){
        if((count % 2) == 0){
          parid = 'odd';
        }else{
          parid = 'even';
        }
        var sesion = '<tr class="'+parid+' pointer sesion"> <td>'+(count+1)+'</td> <td>'+obj.sesiones[i].nombre+'</td><td>'+obj.sesiones[i].descripcion+'</td> <td>'+obj.sesiones[i].fechaHora+'</td>  </tr>';    
        $('#table-sesiones').children().next().append(sesion);        
        count++;
      }
      count = 0;
      for (i in obj.colaboradores ){
        if((count % 2) == 0){
          parid = 'odd';
        }else{
          parid = 'even';
        }
        var colaborador = '<tr class="'+parid+' pointer colab"> <td>'+(count+1)+'</td> <td>'+obj.colaboradores[i].nombre+'</td> <td>'+obj.colaboradores[i].apellido+'</td> <td>'+obj.colaboradores[i].tipoIdentificacion+'</td> <td>'+obj.colaboradores[i].identificacion+'</td> <td>'+obj.colaboradores[i].tipo+'</td> <td>'+obj.colaboradores[i].unidadAcademica.nombre+'</td> <td class="none">'+obj.colaboradores[i].id+'</td>  <input type="hidden" value="'+obj.colaboradores[i].id+'" class="idColab"> </tr>';
        $('#table-colab').children().next().append(colaborador);
        count++;
      }
      count = 0;      
      for (i in obj.beneficiarios ){
        if((count % 2) == 0){
          parid = 'odd';
        }else{
          parid = 'even';
        }
          var beneficiario = '<tr class="'+parid+' pointer benef"> <td>'+(count+1)+'</td> <td>'+obj.beneficiarios[i].nombre+'</td> <td>'+obj.beneficiarios[i].apellido+'</td> <td>'+obj.beneficiarios[i].tipoIdentificacion+'</td> <td>'+obj.beneficiarios[i].identificacion+'</td> <td>'+((obj.beneficiarios[i].sexo == 'M') ? 'Masculino' : 'Femenino' )+'</td> <td>'+obj.beneficiarios[i].edad+'</td> </tr>';
      $('#table-benef').children().next().append(beneficiario);
        count++;
      }      
      
    
    }
  );


});