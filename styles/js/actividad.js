$(document).ready(function(){
  function inputTextValidation(validacionTab){
    var val = true;    
    $('.text-danger').remove();
    $(validacionTab).each(function(){
      var valor = $(this).val();
      if(valor == ''){      
        $(this).after('<p class="text-danger">Este campo es obligatorio</p>');
        val = false;
      }      
    });
    return val;
  }

  function tableEmpty(element, table){
    $('.alert-dismissible').remove();    
    var count = 0;
    $(element).each(function(){      
      count++;
    });
    if(count == 0){      
      $(table).after('<div class="alert alert-danger alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span> </button> Debe agregar al menos un elemento para la actividad </div>');
      return false;
    }else{
      return true;      
    }      
  }

  function dateTimeFormat(date, time){
  
     var age = date.substring(6, 10);
     var month = date.substring(0, 2);   
     var day =  date.substring(3, 5);

     var hour = time.substring(0, 2);
     var minute = time.substring(3, 5);

     var dateTime = age+'-'+month+'-'+day+' '+hour+'-'+minute;
    return dateTime;     
  }

  function dateFormat(date){
     var age = date.substring(6, 10);
     var month = date.substring(0, 2);   
     var day =  date.substring(3, 5);

     var dateFormat = age+'-'+month+'-'+day;
    return dateFormat;     
  }

  function countElementsTable(table){
    var count = 0;
    $(table).each(function(){
      count++;
    });
    return count;     
  }

  $('.validacion-tab1').change(function() {
    $(this).next().fadeOut(function(){
      $(this).next().remove();  
    });    
  });

  /*function responsSelected(){
    $('.resp-danger').remove();
    var resp = $('#responsable').text()
    if(resp == ''){
      $('#responsable').parent().after('<p class="text-danger resp-danger"> Debe indicar un responsable para la actividad</p>');
      return true;
    }else{
      return false;
    }
  }*/
  
  $('#add-sesion').click(function() {
  
    var valid = true;
    var nombre = $('#nombre-sesion').val();
    var descripcion = $('#descripcion-sesion').val();
    var lugar = $('#lugar-sesion').val();
    $('#add-sesion').next().remove();
    $('#nombre-sesion').next().remove();
    $('#descripcion-sesion').next().remove();
    $('#lugar-sesion').next().remove();
    if(nombre != '' && descripcion != '' && lugar != ''){
      $('.alert-dismissible').remove();        
      var fecha = $('#single_cal4').val();
      var hora = $('#hora-sesion').val();
      var duracion = $('#duracion-sesion').val();
      var num = countElementsTable('.sesion');
      // Validacion de temporalidad 
      // if( sucecionSesion(fecha, hora) ){         
        var parid;
        if((num % 2) == 0){
          parid = 'odd';
        }else{
          parid = 'even';
        }
        var sesion = '<tr class="'+parid+' pointer sesion"> <td>'+(num+1)+'</td> <td>'+nombre+'</td><td>'+descripcion+'</td><td>'+lugar+'</td> <td>'+fecha+'</td> <td>'+hora+'</td> <td>'+duracion+'</td><td> <i class="fa fa-remove close-sesion"></i></td> </tr>';    
        $('#table-sesiones').children().next().append(sesion);
        $('#nombre-sesion').next().remove();
        $('#descripcion-sesion').next().remove();
        $('#nombre-sesion').val('');
        $('#descripcion-sesion').val('');
        $('#lugar-sesion').val('');
      // Validacion de temporalidad 
      // }else{        
      //   $('#add-sesion').after('<p class="text-danger ">La sesion debe ser realizada despues de la agregada previamente</p>');
      // }
      }else{        
        $('.input-sesion').each(function(){          
          if($(this).val() == ''){          
            $(this).after('<p class="text-danger ">Este campo es obligatorio</p>');
          }
        });
      }
  });

  $('.tempo').change(function(){
    $('#add-sesion').next().remove();
  });

  function sucecionSesion(fecha, hora){
    var valid = true;
    if(numSesiones() > 0){
      var fechaInical = new Date(obtenerUltimaFechaHoraSesion());
      var fechaFinal = new Date(fecha + ' ' + hora);      
      if(fechaInical < fechaFinal){          
        return true;
      }else{
        return false;
      }
    }else{
      return true;
    }    
  }

  function obtenerUltimaFechaHoraSesion(){
    var fechaHora;
    var num = numSesiones();
    var count = 1;
    $('.sesion').each(function() {                  
      if(num == count){        
        fechaHora = $(this).children().eq(3).text() + ' ' + $(this).children().eq(4).text();        
      }
      count++;
    }); 
    return fechaHora;
  }

  function numSesiones(){
    var count = 0;
    $('.sesion').each(function() {      
      count++;
    }); 
    return count;
  }

  /*
  function sesiones(){    
    var list = new Array();
    var sesion;
    $('.sesion').each(function() {      
        sesion = { 
          "numero":$(this).children().eq(0).text(), 
          "nombre":$(this).children().eq(1).text(), 
          "descripcion":$(this).children().eq(2).text(), 
          "fechaHora": dateTimeFormat( $(this).children().eq(3).text() ,  $(this).children().eq(4).text() ),          
          "duracion":$(this).children().eq(5).text()
        }
        list.push(sesion);                    
    }); 
    return list;
  }
  */

  $('#nombre-sesion').change(function(){
    $(this).next().remove();
  });

  $('#descripcion-sesion').change(function(){
    $(this).next().remove();
  });

  $(".list-sesiones").on("click", ".close-sesion", function(){            
      $(this).parent().parent().remove();  
      reasignNumberItems('.sesion');    
  });

  function reasignNumberItems(item){
    var count = 1;
    $(item).each(function() {
      $(this).children().first().text(count);
      count++;
    });
  }

  $('#select-resp').click(function() {
    $('.resp-danger').remove();    
    var resp = $('#responsables option:selected').text();
    var idResp = $('#responsables option:selected').val();
    
    $('#responsable').text(resp);
    $('#idResponsable').val(idResp);
  });
  
  function tipoConvocatoria(){
    var convocatoria = $("input[name='convocatoria']:checked").val();
    if(convocatoria == 'Especifica'){
      $('#tab-especifica').css('display','block');
      $('#tab-masiva').css('display','none');      
    }
    if(convocatoria == 'Masiva'){
      $('#tab-masiva').css('display','block');
      $('#tab-especifica').css('display','none');
    }    
    return convocatoria;
  }


  $('#atr-tabHome').click(function() {
    $('#home-tab').click();
  });



  $(".list-benef").on("click", ".close-benef", function(){            
      $(this).parent().parent().remove();  
      reasignNumberItems('.benef');    
  });

  $('#nombre-beneficiario').change(function() {
    $(this).next().remove();
  });

  $('#documento').change(function() {
    $(this).next().remove();
  });

  $('#atr-profileTab').click(function() {
    $('#profile-tab').click();
  });


  $('#sig-tab1').click(function(){          
    var validation = true;
    if( !inputTextValidation('.validacion-tab1') ){      
      validation = false;
    }    
    if(!tableEmpty('.sesion', '#table-sesiones')){    
      validation = false;
    }
    if(validation){      
      $('#tab2').css('display','block');
      
      $('#profile-tab').click();  
    }else{
      $('#tab2').css('display','none');
      $('#tab3').css('display','none');
    }    
  });

  $('#sig-tab2').click(function() {
    var validation = true;
    if(!tableEmpty('.colab', '#table-colab')){    
      validation = false;
    }
    if(validation){
      $('#profile-tab2').click();      
      $('#tab3').css('display','block');
    }else{
      $('#tab3').css('display','none');
    }
  });  

  $('#sig-tab3').click(function(){    
    if( tipoConvocatoria() == 'Especifica'){    
      if(tableEmpty('.benef', '#table-benef')){         
        crearActividad();  
      }
    }else if(  tipoConvocatoria() == 'Masiva'){          
      crearActividad();
    } 
  });
  
  function crearActividad(){

    var nombre = $('#nombre').val();
    var objetivo = $('#objetivo').val();
    var tipo = $('#tipo').val();
    var convocatoria = $("input[name='convocatoria']:checked").val();
    var listSesiones = sesiones();        
    var resultado =  $('#summernote').summernote('code');
    resultado = resultado.replace(/"/g, "'");    
    var listColab = colaboradores();
    var listBenef;
    var volumen;
    if( tipoConvocatoria() == 'Especifica'){
      listBenef = beneficiarios();  
      volumen = listBenef.length;
    }else if(  tipoConvocatoria() == 'Masiva'){          
      volumen = $('#volumen').val();
    }   
    var obj = '{"nombre":"'+nombre+'", "objetivo":"'+objetivo+'", "tipo":"'+tipo+'", "convocatoria":"'+convocatoria+'", "idResponsable":"'+idUsuario+'", "volumen":"'+volumen+'", "resultado":"'+resultado+'", "creador":"'+idUsuario+'", "sesiones": [';        
    for (var i = 0; i < listSesiones.length; i++) {
      if((i+1) == listSesiones.length){
        obj += '{ "numero":"'+listSesiones[i].numero+'", "nombre":"'+listSesiones[i].nombre+'", "descripcion":"'+listSesiones[i].descripcion+'", "lugar":"'+listSesiones[i].lugar+'", "fechaHora":"'+listSesiones[i].fechaHora+'", "duracion":"'+listSesiones[i].duracion+'"}';  
      }else{          
        obj += '{ "numero":"'+listSesiones[i].numero+'", "nombre":"'+listSesiones[i].nombre+'", "descripcion":"'+listSesiones[i].descripcion+'", "lugar":"'+listSesiones[i].lugar+'", "fechaHora":"'+listSesiones[i].fechaHora+'", "duracion":"'+listSesiones[i].duracion+'"},';  
      }      
    }

    if( tipoConvocatoria() == 'Especifica'){
      obj += '], "beneficiarios": [';
      for (var i = 0; i < listBenef.length; i++) {
        if((i+1) == listBenef.length){
          obj += '{ "numero":"'+listBenef[i].numero+'", "nombre":"'+listBenef[i].nombre+'", "apellido":"'+listBenef[i].apellido+'", "tipoIdentificacion":"'+listBenef[i].tipoIdentificacion+'", "identificacion":"'+listBenef[i].identificacion+'", "sexo":"'+listBenef[i].sexo+'", "edad":"'+listBenef[i].edad+'"}';  
        }else{
          obj += '{ "numero":"'+listBenef[i].numero+'", "nombre":"'+listBenef[i].nombre+'", "apellido":"'+listBenef[i].apellido+'", "tipoIdentificacion":"'+listBenef[i].tipoIdentificacion+'", "identificacion":"'+listBenef[i].identificacion+'", "sexo":"'+listBenef[i].sexo+'", "edad":"'+listBenef[i].edad+'"},';  
        }      
      }
    }


    obj += '], "colaboradores": [';
    for (var i = 0; i < listColab.length; i++) {
      if((i+1) == listColab.length){
        obj += '{ "id":"'+listColab[i].id+'", "nombre":"'+listColab[i].nombre+'", "apellido":"'+listColab[i].apellido+'", "tipoIdentificacion":"'+listColab[i].tipoIdentificacion+'", "identificacion":"'+listColab[i].identificacion+'", "email":"'+listColab[i].email+'", "tipo":"'+listColab[i].tipo+'", "idUnidadAcademica":"'+listColab[i].idUnidadAcademica+'", "sexo":"'+listColab[i].sexo+'" }';  
      }else{
        obj += '{ "id":"'+listColab[i].id+'", "nombre":"'+listColab[i].nombre+'", "apellido":"'+listColab[i].apellido+'", "tipoIdentificacion":"'+listColab[i].tipoIdentificacion+'", "identificacion":"'+listColab[i].identificacion+'", "email":"'+listColab[i].email+'", "tipo":"'+listColab[i].tipo+'", "idUnidadAcademica":"'+listColab[i].idUnidadAcademica+'", "sexo":"'+listColab[i].sexo+'" },';  
      }      
    }
    
    obj += ']}';   
    console.log(obj);
    $.post(
      "Controllers/AjaxController.php?funcion=crearActividad",            
      {
          json : obj                
      },
      function(data,status){       
        
        if(data != 0){
          swal({                
            title: "Actividad Creada",
            text: "La actividad ha sido creada exitosamente, los detalles se mostraran a continuacion ",
            type: "success"
          }, function(){
            window.location.href = 'actividad.php?idActividad='+data;                      
          }); 
        }else{
          swal({                
            title: "Error",
            text: "Ha ocurrido un error al crear la actividad",
            type: "error"
          }); 
        }
      }
    );  
  }


  $('#profile-tab2').click(function() {
    tipoConvocatoria();
  });

  var now = new Date();
  var month = (now.getMonth() + 1);               
  var day = now.getDate();
  if(month < 10) 
      month = "0" + month;
  if(day < 10) 
      day = "0" + day;
  var today =  month + '/' + day + '/' + now.getFullYear();
  $('#single_cal1').val(today);    
  $('#single_cal4').val(today);

  function sesiones(){    
    var list = new Array();
    var sesion;
    $('.sesion').each(function() {      
        sesion = { 
          "numero":$(this).children().eq(0).text(), 
          "nombre":$(this).children().eq(1).text(), 
          "descripcion":$(this).children().eq(2).text(), 
          "lugar":$(this).children().eq(3).text(), 
          "fechaHora": dateTimeFormat( $(this).children().eq(4).text() ,  $(this).children().eq(5).text() ),          
          "duracion":$(this).children().eq(6).text()
        }        
        list.push(sesion);                    
    }); 
    return list;
  }

  function beneficiarios(){    
    var list = new Array();
    var benef;
    $('.benef').each(function() {      
        sesion = { 
          "numero":$(this).children().eq(0).text(), 
          "nombre":$(this).children().eq(1).text(), 
          "apellido":$(this).children().eq(2).text(), 
          "tipoIdentificacion":$(this).children().eq(3).text(),
          "identificacion":$(this).children().eq(4).text(),
          "sexo":( ( $(this).children().eq(5).text() == 'Masculino' ) ? 'M' : 'F'),
          "edad":$(this).children().eq(6).text()
        }
        list.push(sesion);                    
    }); 
    return list;
  }
  
// ======================================

  $.get("Controllers/AjaxController.php?funcion=tiposColaborador", 
    function(data, status){            
      if(data != 0){
        var obj = JSON.parse(data);
        var option = '';
        for (i in obj){
          option += '<option>'+obj[i]+'</option>';        
        }
        $('#tipo-colaborador').append(option);    
        $('#tipo-responsable').append(option);                  
      }
    }
  );
  
  function unidadesAcademicas(tipo, selector){        
    $.post(
      "Controllers/AjaxController.php?funcion=unidadesAcademicasTipo",            
      {
          tipo: tipo          
      },
      function(data,status){                         
        if(data != 0){          
          var obj = JSON.parse(data);             
          var option = '';  
          for (i in obj){
            option += '<option value="'+obj[i]['id']+'">'+obj[i]['nombre']+'</option>';        
          }        
          $(selector).append(option);
        }                           
      }
    );
  }

  unidadesAcademicas('Programa', '#unidadAcademicaCol');

  var identColabSelected = false;
  $('#identificacionCol').click(function(){
    identColabSelected = true;
    identBenefSelected = false;
  });

  $('#identificacionCol').change(function(){    
    buscarColaborador(0, false);  
    identColabSelected = false;
  });  

  $('#tipoIdentCol').change(function(){
    buscarColaborador(0, false);
    identColabSelected = false;
  });

  $('#documento').change(function(){
    buscarBeneficiario(0, false);
    identBenefSelected = false;
  });

  var identBenefSelected = false;
  $('#documento').click(function(){
    identBenefSelected = true;
    identColabSelected = false;
  });

  $('#tipo-documento').change(function(){
    buscarBeneficiario();
    identBenefSelected = false;
  });

  $(document).keypress(function(e) {
    var lastKey = String.fromCharCode(e.keyCode);      
    if(identColabSelected){
      buscarColaborador(lastKey, true);  
    }     
    if(identBenefSelected){
      buscarBeneficiario(lastKey, true);  
    }
  });


  function buscarColaborador(lastKey, keypress){
    var identificacion = $('#identificacionCol').val();
    if(keypress){
      identificacion = identificacion + lastKey;
    }
    var tipoIdent = $('#tipoIdentCol').val();
    
    var permiso = 'Colaborador';
    $('.inputHidenCol').fadeIn();    
    $.post(
      "Controllers/AjaxController.php?funcion=consultarColaboador",            
      {
          tipoIdent : tipoIdent,
          identificacion: identificacion,
          permiso : permiso       
      },
      function(data,status){        
        if(data != 0){
           var obj = JSON.parse(data);
           $('#nombreCol').val(obj.nombre);
           $('#apellidoCol').val(obj.apellido);
           $('#sexoCol').val(obj.sexo);
           $('#emailCol').val(obj.email);
           $('#tipoCol').val(obj.tipo);           
           $('#unidadAcademicaCol').val(obj.unidadAcademica);      
           $('#idCol').text(obj.id);
           $('#add-colab').text('');
           $('#add-colab').append('<i class="fa fa-plus"></i> Agregar colaborador');
           $('.inputCol').attr('disabled', 'disabled');     
           $('.text-danger').remove();   
        }else{
           setDefaultInputsColab();
        }
      }
    );
  }


  function buscarBeneficiario(lastKey, keypress){
    var identificacion = $('#documento').val();    
    if(keypress){
      identificacion = identificacion + lastKey;
    }
    var tipoIdent = $('#tipo-documento').val();
    
    $('.inputHidenBenef').fadeIn();    
    $.post(
      "Controllers/AjaxController.php?funcion=consultarBeneficiario",            
      {
          tipoIdent : tipoIdent,
          identificacion: identificacion          
      },
      function(data,status){        
        if(data != 0){
           var obj = JSON.parse(data);
           $('#nombre-beneficiario').val(obj.nombre);
           $('#apelido-beneficiario').val(obj.apellido);
           $('#sexo-beneficiario').val(obj.sexo);
           $('#edad-beneficiario').val(obj.edad);         
           $('#idBenef').text(obj.id);
           $('#add-benef').text('');
           $('#add-benef').append('<i class="fa fa-plus"></i> Agregar beneficiario');
           $('.inputBenef').attr('disabled', 'disabled');  
           $('.text-danger').remove();
        }else{
           setDefaultInputsBenef();
        }
      }
    );
  }  
  

  function colaboradores(){
    var list = new Array();
    var colab;
    $('.colab').each(function(){      
        colab = { 
          "nombre":$(this).children().eq(1).text(), 
          "apellido":$(this).children().eq(2).text(), 
          "tipoIdentificacion":$(this).children().eq(3).text(), 
          "identificacion":$(this).children().eq(4).text(), 
          "email":$(this).children().eq(5).text(), 
          "tipo":$(this).children().eq(6).text(), 
          "idUnidadAcademica":$(this).children().eq(7).children().first().text(), 
          "sexo":( ( $(this).children().eq(8).text() == 'Masculino' ) ? 'M' : 'F'),
          "id":$(this).children().eq(9).text()
        }        

        list.push(colab);                    
    }); 
    console.log(list);
    return list;  
  }

  
  function setDefaultInputsColab(){

    $('#nombreCol').val('');
    $('#apellidoCol').val('');    
    $('#emailCol').val('');      
    $('#add-colab').text('');  
    $('#idCol').text('');      
    
    $('#add-colab').append('<i class="fa fa-plus"></i> Crear colaborador');
    $('.inputCol').removeAttr('disabled'); 
  }

  $('#add-colab').click(function(){
      $('.alert-dismssible').remove();
    
      var tipoIdent = $('#tipoIdentCol option:selected').text();
      var identificacion = $('#identificacionCol').val();
      var nombre = $('#nombreCol').val();
      var apellido = $('#apellidoCol').val();
      var sexo = $('#sexoCol option:selected').text();
      var email = $('#emailCol').val();
      var tipo = $("#tipoCol option:selected").text();
      var idUnidad = $("#unidadAcademicaCol option:selected").val();
      var unidad = $("#unidadAcademicaCol option:selected").text();
      var id = $('#idCol').text();

      var num = countElementsTable('.colab');
      var parid;
      if((num % 2) == 0){
        parid = 'odd';
      }else{
        parid = 'even';
      }

      if(inputTextValidation('.inputCol')){
        if(!findDuplicateColab(identificacion, 'identRow') ){     
          $('.inputHidenCol').hide(); 
          var colaborador = '<tr class="'+parid+' pointer colab"> <td>'+(num+1)+'</td> <td>'+nombre+'</td> <td>'+apellido+'</td> <td>'+tipoIdent+'</td> <td class="identRow">'+identificacion+'</td> <td>'+email+'</td> <td>'+tipo+'</td> <td><span hidden>'+idUnidad+'</span><span>'+unidad+'</span></td> <td>'+sexo+'</td> <td class="none">'+id+'</td> <td></i> <i class="fa fa-remove close-colab"></i></td> <input type="hidden" value="'+id+'" class="idColab"> </tr>';
          $('#table-colab').children().next().append(colaborador);
          setDefaultInputsColab();
          
          $('#identificacionCol').val('');
        }else{
          $('#table-colab').parent().append('<div class="alert alert-danger alert-dismssible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span> </button> El colaborador ya ha sido agregado anteriormente.</div>');        
        }
      }
  });

  $(".list-colab").on("click", ".close-colab", function(){
      $(this).parent().parent().remove();  
      reasignNumberItems('.colab');    
  });


  function findDuplicateColab(ident, tipo){    
    var exist = false;
    $('.'+tipo).each(function(){      
      if(new String(ident).valueOf() == new String($(this).text()).valueOf()){        
        exist = true;
        }        
      }
    );
    return exist;
  }

  $('.inputCol').change(function(){
    $(this).next().remove();
  })

  $('#crear-parc').click(function(){

    if(tableEmpty('.colab', '#table-colab')){    
      crearActividad();
    }
  });

//=========================================================================================================================================================//
//==================================================================SEGUNDO NUEVO DESARROLLO=======================================================================//
//=========================================================================================================================================================//

  function setDefaultInputsBenef(){
    $('#nombre-beneficiario').val('');
    $('#apelido-beneficiario').val('');  
    $('#edad-beneficiario').val('');      
    $('#add-benef').text('');  
    $('#idBenef').text('');  
    $('.colab-rep').remove();
    $('#add-benef').append('<i class="fa fa-plus"></i> Crear beneficiario');
    $('.inputBenef').removeAttr('disabled'); 
  }

  $('#add-benef').click(function(){
    $('.alert-dismissible').remove();
    
    var tipoIdent = $('#tipo-documento option:selected').text();
    var identificacion = $('#documento').val();
    var nombre = $('#nombre-beneficiario').val();
    var apellido = $('#apelido-beneficiario').val();
    var sexo = $('#sexo-beneficiario option:selected').text();
    var edad = $('#edad-beneficiario').val();   
    var id = $('#idBenef').text();

    var num = countElementsTable('.benef');
    var parid;
    if((num % 2) == 0){
      parid = 'odd';
    }else{
      parid = 'even';
    }

    if(inputTextValidation('.inputBenef')){
      if(!findDuplicateColab(identificacion, 'identRowBenef') ){                
        var beneficiario = '<tr class="'+parid+' pointer benef"> <td>'+(num+1)+'</td> <td>'+nombre+'</td> <td>'+apellido+'</td> <td>'+tipoIdent+'</td> <td class="identRowBenef">'+identificacion+'</td> <td>'+sexo+'</td> <td>'+edad+'</td> <td class="none">'+id+'</td> <td></i> <i class="fa fa-remove close-benef"></i></td> <input type="hidden" value="'+id+'" class="idBenef"> </tr>';          
        $('#table-benef').children().next().append(beneficiario);
        $(".inputHidenBenef").hide();
        setDefaultInputsBenef();
        $('#documento').val('');
      }else{
        $('#table-benef').parent().append('<div class="alert alert-danger alert-dismssible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span> </button> El beneficiario ya ha sido agregado anteriormente.</div>');                  
      }
    }
  });

  $("#edad-beneficiario").change(function(){
    var edad = $(this).val();
    if(edad < 0 && !edad % 1 == 0 || edad > 110){
      $(this).val('');
    } 
  });

});