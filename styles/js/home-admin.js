
  $.post(
    "Controllers/AjaxController.php?funcion=actividadesAdministrador",            
    {
        id : idUsuario                
    },
    function(data,status){ 
      if(data != 0){          
        var obj = $.parseJSON(data);      
        console.log(obj);
        for (var i in obj) {
          
          if(obj[i].activo == 1){                      
            var resultado;
            if(obj[i].resultado == 'NULL'){
              resultado = 'Pendiente';
            }else{
              resultado = obj[i].resultado;
            }
            $('#resultado').text(resultado);
            $('#estado').text(obj[i].estado);
            var parid;
            var count = 0;
            
            if((count % 2) == 0){
              parid = 'odd';
            }else{
              parid = 'even';
            }
            $('.dataTables_empty').hide();
            var actividad = '<tr><td>'+obj[i].nombre+'</td> <td>'+obj[i].objetivo+'</td> <td>'+obj[i].tipo+'</td> <td>'+obj[i].convocatoria+'</td> <td> <a class="ver-actividad" href="actividad.php?idActividad='+obj[i].id+'"><span class="label label-info"><i class="fa fa-eye"></i></span></a> <a class="editar-actividad" href="editar.php?idActividad='+obj[i].id+'"><span class="label label-success"><i class="fa fa-edit"></i></span></a> <a class="eliminar-actividad"><span class="label label-danger"><i class="fa fa-trash"></i></span></a> <span id="'+obj[i].id+'" hidden>'+obj[i].id+'</span> </td></tr>';
            $('.table-actividad-creadas').append(actividad);                   
            count++;
          }
        }
        $('.dataTables-example1').DataTable({
          pageLength: 25,
          responsive: true,
          dom: '<"html5buttons"B>lTfgitp',
          buttons: [
              { extend: 'copy'},
              {extend: 'csv'},
              {extend: 'excel', title: 'Actividades'},
              {extend: 'pdf', title: 'Actividades'},

              {extend: 'print',
                customize: function (win){
                      $(win.document.body).addClass('white-bg');
                      $(win.document.body).css('font-size', '10px');

                      $(win.document.body).find('table')
                              .addClass('compact')
                              .css('font-size', 'inherit');
                }
                }
            ]
        });
      }
    }           
  );

 

  $(".table-actividad-creadas").on("click", ".eliminar-actividad", function(){     
      var idACtivdad = $(this).next().text();      
      swal({
          title: "Esta seguro?",
          text: "La actividad se eliminara",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, Eliminar",
          closeOnConfirm: false
      }, function () {        
          $.post(
            "Controllers/AjaxController.php?funcion=eliminarActividad",            
            {
                id : idACtivdad                
            },
            function(data,status){                           
              if(data == 1){                                
                swal("Elimanada", "La actividad ha sido eliminada", "success");
                $('#'+idACtivdad).parent().parent().fadeOut(function(){
                  $(this).remove();
                });
              }else{
                swal("Error", "Ha habido un error al eliminar la actividad, intentelo mas tarde", "error");          
              }                                      
          });      
      });

  });

  $('#off-activities').click(function(){
     swal({
          title: "Esta seguro?",
          text: "Las actividades del actual semestre se desactivaran",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, Desactivar",
          closeOnConfirm: false
      }, function () {        
          $.post(
            "Controllers/AjaxController.php?funcion=desactivarActivades",            
            {
                id : 1                
            },
            function(data,status){                           
              if(data == 1){    
              swal({                
                  title: "Actividades desactivadas",
                  text: "Las actividades ha sido desactivadas exitosamente",
                  type: "success"
                }, function(){
                  window.location.href = 'home.php';  
                });                                                          
              }else{
                swal("Error", "Ha habido un error al desactivar las actividades, intentelo mas tarde", "error");          
              }                                      
          });      
      });
  });


 

