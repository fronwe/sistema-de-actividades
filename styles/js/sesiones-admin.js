
  $.post(
    "Controllers/AjaxController.php?funcion=actividadesAdministrador",            
    {
        id : idUsuario                
    },
    function(data,status){ 
          
  	  var obj = $.parseJSON(data);            
      for (var i in obj) {
          
        $('#estado').text(obj[i].estado);        

        var parid;
        var count = 0;
        
        if((count % 2) == 0){
          parid = 'odd';
        }else{
          parid = 'even';
        }        
        $('.dataTables_empty').hide();
        for (var j in obj[i].sesiones) {                    
          var actividad = '<tr><td>'+obj[i].sesiones[j].fechaHora+'</td> <td>'+obj[i].tipo+'</td> <td>'+obj[i].nombre+'</td> <td>'+obj[i].sesiones[j].nombre+'</td> <td>'+obj[i].responsable.nombre + ' ' + obj[i].responsable.apellido+'</td> <td>'+obj[i].convocatoria+'</td><td>'+obj[i].sesiones[j].duracion+'</td></tr>';          
          $('.table-actividad-creadas').append(actividad);
        }                                               
        count++;        
      }
      $('.dataTables-example1').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'Actividades'},
                    {extend: 'pdf', title: 'Actividades'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });
    }           
  );


 

