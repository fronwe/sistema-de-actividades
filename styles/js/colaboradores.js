  $('.panel-crear-colaborador').fadeIn();

  $.get(
    "Controllers/AjaxController.php?funcion=colaboradores",            
    function(data,status){             
      if(data != 0){    
        $('.dataTables_empty').remove();
        var obj = $.parseJSON(data);                    
        var reasignar = '';
        
        for (var i in obj) {  
          if( obj[i].activo == 1 ){
            if(md5(obj[i].identificacion) != obj[i].clave){            
              reasignar = '<a class="clave-colaborador"><span class="label label-default"><i class="fa fa-eraser"></i></span></a> ';
            }
            var colaborador = '<tr> <td><span>'+obj[i].unidadAcademica.nombre+'</span><span hidden>'+obj[i].unidadAcademica.id+'</span></td> <td>'+obj[i].nombre+'</td> <td>'+obj[i].apellido+'</td> <td>'+obj[i].email+'</td> <td>'+((obj[i].sexo == 'M') ? "Masculino" : "Femenino")+'</td> <td>'+ obj[i].tipo +'</td> <td>'+obj[i].tipoIdentificacion+'</td> <td>'+obj[i].identificacion+'</td>  <td> <span id="'+obj[i].id+'" hidden>'+obj[i].id+'</span>   <a class="editar-colaborador"><span class="label label-success"><i class="fa fa-edit"></i></span></a> <a class="eliminar-colaborador"><span class="label label-danger"><i class="fa fa-times"></i></span></a>   '+ reasignar +'   </td></tr>';
            $('.table-colaboradores').append(colaborador);            
            reasignar = '';               
          }                                
        }

        $('.dataTables-example').DataTable({
          pageLength: 25,
          responsive: true,
          dom: '<"html5buttons"B>lTfgitp',
          buttons: [
            { extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Responsables'},
            {extend: 'pdf', title: 'Responsables'},

            {extend: 'print',
              customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
              }
            }
          ]
        });
      }           
    }
  );

  function unidadesAcademicas(tipo, selector){        
    $.post(
      "Controllers/AjaxController.php?funcion=unidadesAcademicasTipo",            
      {
          tipo: tipo          
      },
      function(data,status){                         
        if(data != 0){          
          var obj = JSON.parse(data);             
          var option = '';  
          for (i in obj){
            option += '<option value="'+obj[i]['id']+'">'+obj[i]['nombre']+'</option>';        
          }        
          $(selector).append(option);
        }                           
      }
    );
  }

  unidadesAcademicas('Departamento', '#unidad');
  unidadesAcademicas('Departamento', '  #nUnidad');

  function inputTextValidation(validacionTab){
    var val = true;    
    $('.text-danger').remove();
    $(validacionTab).each(function(){
      var valor = $(this).val();
      if(valor == ''){      
        $(this).after('<p class="text-danger">Este campo es obligatorio</p>');
        val = false;
      }      
    });
    return val;
  }

  $('.input-text').change(function(){
    $(this).next().fadeOut(function(){
      $(this).remove();
    });
  });

  $('#crear').click(function(){    
    if(inputTextValidation('.input-text')){
      var nombre = $('#nombre').val();      
      var email = $('#email').val();
      var apellido = $('#apellido').val();
      var sexo = $('#sexo').val();
      var email = $('#email').val();      
      var tipo = $('#tipo').val();
      var unidad = $('#unidad').val();
      var identificacion = $('#identificacion').val();
      var tipoIdentificacion = $('#tipoIdentificacion').val();
      var usuario = 'Responsable';
      var clave = $('#identificacion').val();
      var hash = md5(clave);
      var obj = '{"nombre":"'+nombre+'", "apellido":"'+apellido+'", "sexo":"'+sexo+'", "email":"'+email+'", "clave":"'+hash+'", "tipo":"'+tipo+'", "unidad":"'+unidad+'", "identificacion":"'+identificacion+'", "tipoIdentificacion":"'+tipoIdentificacion+'" , "usuario":"'+usuario+'"}';
      
      $.post(
        "Controllers/AjaxController.php?funcion=crearColaborador",
        {
            colaborador : obj                
        },
        function(data,status){                   
          if(data != 0){
            swal({                
              title: "Responsable Agregado",
              text: "El responsable ha sido agregado exitosamente, los detalles se mostraran a continuacion ",
              type: "success"
            }, function(){
              window.location.href = 'colaboradores.php';  
            });       
          }else{
            swal({                
              title: "Error",
              text: "Ha ocurrido un error al agregar al responsable, Intentelo de nuevo",
              type: "error"
            }); 
          }
        }           
      );
    }
  });

  $(".table-colaboradores").on("click", ".eliminar-colaborador", function(){     
      var idColaborador = $(this).prev().prev().text();      
      swal({
          title: "Esta seguro?",
          text: "El responsable se eliminara",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, Eliminar",
          closeOnConfirm: false
      }, function () {        
          $.post(
            "Controllers/AjaxController.php?funcion=eliminarColaborador",            
            {
                id : idColaborador
            },
            function(data,status){                                       
              if(data == 1){                                
                swal({                
                  title: "Responsable Eliminado",
                  text: "El responsable ha sido eliminado exitosamente",
                  type: "success"
                }, function(){
                  window.location.href = 'colaboradores.php';  
                });              
              }else{
                swal("Error", "Ha habido un error al eliminar el responsable, intentelo de nuevo", "error");          
              }                                      
          });      
      });
  });

  $(".table-colaboradores").on("click", ".editar-colaborador", function(){            
    $('.panel-crear-colaborador').fadeOut(function(){
      $('.panel-editar-colaborador').fadeIn();
    });

    var idColaborador = $(this).prev().text();         
    var nUnidadAcademica = $(this).parent().parent().children().eq(0).children().first().text(); 
    var nIdUnidadAcademica = $(this).parent().parent().children().eq(0).children().next().text(); 
    var nNombre = $(this).parent().parent().children().eq(1).text();     
    var nApellido = $(this).parent().parent().children().eq(2).text();         
    var nEmail = $(this).parent().parent().children().eq(3).text(); 
    var nSexo = ( ( $(this).parent().parent().children().eq(4).text() == 'Masculino' ) ? 'M' : 'F');     
    var nTipo = $(this).parent().parent().children().eq(5).text(); 
    var nTipoIdentificacion = $(this).parent().parent().children().eq(6).text(); 
    var nIndetificacion = $(this).parent().parent().children().eq(7).text(); 
                
    $('#id-colaborador').val(idColaborador);
    $('#nNombre').val(nNombre);
    $('#nApellido').val(nApellido);
    $('#nSexo').val(nSexo);
    $('#nEmail').val(nEmail);    
    $('#nTipoIdentificacion').val(nTipoIdentificacion);    
    $('#nIdentificacion').val(nIndetificacion);
    $('#nTipo').val(nTipo);
    $('#nUnidadAcademica').val(nUnidadAcademica);    

    $("html, body").animate({ scrollTop: "1000px" });
  });

  $(".table-colaboradores").on("click", ".clave-colaborador", function(){            
   
    var idColaborador = $(this).prev().prev().prev().text();    
    var identificacion = $(this).parent().parent().children().eq(7).text();     
    var hashIdent = md5(identificacion);    
    swal({
        title: "Esta seguro?",
        text: "La contraseña del responsable se restaurara",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, restaurar",
        closeOnConfirm: false
    }, function () {        
        $.post(
          "Controllers/AjaxController.php?funcion=restaurarClaveColaborador",            
          {
              id : idColaborador,
              identificacion : hashIdent
          },
          function(data,status){                              
            if(data == 1){                                
              swal({                
                title: "Clave restaurada",
                text: "La contraseña del responsable ha sido restaurada exitosamente",
                type: "success"
              }, function(){
                window.location.href = 'colaboradores.php';  
              });              
            }else{
              swal("Error", "Ha habido un error al restaurar la contraseña del responsable, intentelo de nuevo", "error");          
            }                                      
        });      
    });




  });  

$('#editar').click(function(){  
    if(inputTextValidation('.input-text-edit')){
      var idColaborador = $('#id-colaborador').val();    
      var nombre = $('#nNombre').val();
      var apellido = $('#nApellido').val();
      var sexo = $('#nSexo').val();    
      var email = $('#nEmail').val();      
      var tipo = $('#nTipo').val();
      var unidad = $('#nUnidad').val();
      var identificacion = $('#nIdentificacion').val();
      var tipoIdentificacion = $('#nTipoIdentificacion').val();      
      var obj = '{"id":"'+idColaborador+'", "nombre":"'+nombre+'", "apellido":"'+apellido+'", "sexo":"'+sexo+'", "email":"'+email+'", "tipo":"'+tipo+'", "unidad":"'+unidad+'", "identificacion":"'+identificacion+'", "tipoIdentificacion":"'+tipoIdentificacion+'"}';      
      $.post(
        "Controllers/AjaxController.php?funcion=editarColaborador",            
        {
            colaborador : obj                
        },
        function(data,status){                  
          if(data != 0){
            swal({                
              title: "Responsable Editado",
              text: "El responsable ha sido editado exitosamente",
              type: "success"
            }, function(){
              window.location.href = 'colaboradores.php';  
            });       
          }else{
            swal({                
              title: "Error",
              text: "Ha ocurrido un error al editar al responsable, Intentelo de nuevo",
              type: "error"
            }); 
          }
        }           
      );
    }
});

$('#cancelar').click(function(){
  $('.panel-editar-colaborador').fadeOut(function(){
    $('.panel-crear-colaborador').fadeIn();
  });
});


 

