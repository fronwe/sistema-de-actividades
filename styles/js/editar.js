$(document).ready(function(){    
  $.post(
    "Controllers/AjaxController.php?funcion=verActividad",            
    {
        id : idActividad                
    },
    function(data,status){            

      var obj = $.parseJSON(data);

      $('#nombre').val(obj.nombre);
      $('#objetivo').text(obj.objetivo);
      $('#tipo').val(obj.tipo);

      if(obj.convocatoria == 'Masiva'){
        $('#masiva').prop('checked', true);
        $('#masiva').click();        
        $('#tab-especifica').css('display','none');
        $('#tab-masiva').css('display','block');      
        $('#volumen').val(obj.volumen);

      }else if(obj.convocatoria == 'Especifica'){
        $('#convocatoria').prop('checked', true);
        $('#convocatoria').click();        
        $('#tab-masiva').css('display','none');
        $('#tab-especifica').css('display','block');        
      }


      var parid;
      var count = 0;
      for (i in obj.sesiones ){
        if((count % 2) == 0){
          parid = 'odd';
        }else{
          parid = 'even';
        }
        var sesion = '<tr class="'+parid+' pointer sesion"> <td>'+(count+1)+'</td> <td>'+obj.sesiones[i].nombre+'</td><td>'+obj.sesiones[i].descripcion+'</td> <td>'+obj.sesiones[i].lugar+'</td> <td>'+UnDateFormat( obj.sesiones[i].fechaHora.substring(0, 10))+'</td> <td>'+obj.sesiones[i].fechaHora.substring(10, 16)+'</td> <td>'+obj.sesiones[i].duracion+'</td> <td> <i class="fa fa-remove close-sesion"></i></td> </tr>';    
        $('#table-sesiones').children().next().append(sesion);        
        count++;
      }            
      
      $('#idResponsable').text(obj.responsable.id);
      $('#responsable').val(obj.responsable.nombre);

      if(obj.estado == 'Pendiente'){
        $('#pendiente').prop('checked', true);
      }else if(obj.estado == 'Realizada'){
        $('#realizada').prop('checked', true);
      }      


      if(obj.resultado != 'NULL'){
        $('#resultado').text();
      }else if(obj.resultado == 'Realizada'){
        $('#realizada').prop('checked', true);
      }            
      

      $('#responsable').text(obj.responsable.nombre +' - '+ obj.responsable.unidadAcademica.nombre);

      count = 0;
      for (i in obj.colaboradores ){
        if((count % 2) == 0){
          parid = 'odd';
        }else{
          parid = 'even';
        }

        var colaborador = '<tr class="'+parid+' pointer colab"> <td>'+(count+1)+'</td> <td>'+obj.colaboradores[i].nombre+'</td> <td>'+obj.colaboradores[i].apellido+'</td> <td>'+obj.colaboradores[i].tipoIdentificacion+'</td> <td class="identRow">'+obj.colaboradores[i].identificacion+'</td>   <td>'+obj.colaboradores[i].email+'</td> <td>'+obj.colaboradores[i].tipo+'</td> <td><span hidden>'+obj.colaboradores[i].unidadAcademica.id+'</span><span>'+obj.colaboradores[i].unidadAcademica.nombre+'</span></td>  <td>'+ ( ( obj.colaboradores[i].sexo == 'M') ? 'Masculino' : 'Femenino' ) +'</td> <td class="none">'+obj.colaboradores[i].id+'</td> <td></i> <i class="fa fa-remove close-colab"></i></td> <input type="hidden" value="'+obj.colaboradores[i].id+'" class="idColab"> </tr>';
        $('#table-colab').children().next().append(colaborador);
        count++;
      }      
      
      $('#convocatoria').text(obj.convocatoria);
      $('#estado').text(obj.estado);          
      $('.note-editable').html(obj.resultado);



      count = 0;      
      for (i in obj.beneficiarios ){
        if((count % 2) == 0){
          parid = 'odd';
        }else{
          parid = 'even';
        }
        var beneficiario = '<tr class="'+parid+' pointer benef"> <td>'+(count+1)+'</td> <td>'+obj.beneficiarios[i].nombre+'</td> <td>'+obj.beneficiarios[i].apellido+'</td> <td>'+obj.beneficiarios[i].tipoIdentificacion+'</td> <td class="identRowBenef">'+obj.beneficiarios[i].identificacion+'</td> <td>'+  ( ( obj.beneficiarios[i].sexo == 'M') ? 'Masculino' : 'Femenino' ) +'</td> <td>'+obj.beneficiarios[i].edad+'</td> <td><i class="fa fa-remove close-benef"></i></td> </tr>';
        $('#table-benef').children().next().append(beneficiario);
        count++;
      }     
      
    
    }
  );

  $('#editar').click(function(){    
    if( tipoConvocatoria() == 'Especifica'){    
      if(tableEmpty('.benef', '#table-benef')){         
        editarActividad();  
      }
    }else if(  tipoConvocatoria() == 'Masiva'){          
      editarActividad();
    } 
  });


  function tipoConvocatoria(){
    var convocatoria = $("input[name='convocatoria']:checked").val();
    if(convocatoria == 'Especifica'){
      $('#tab-especifica').css('display','block');
      $('#tab-masiva').css('display','none');      
    }
    if(convocatoria == 'Masiva'){
      $('#tab-masiva').css('display','block');
      $('#tab-especifica').css('display','none');
    }    
    return convocatoria;
  }

  function tableEmpty(element, table){
    $('.alert-dismissible').remove();    
    var count = 0;
    $(element).each(function(){      
      count++;
    });
    if(count == 0){      
      $(table).after('<div class="alert alert-danger alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button> Debe agregar al menos un elemento para la actividad </div>');
      return false;
    }else{
      return true;      
    }      
  }


  function editarActividad(){

    var id = idActividad;
    var idResponsable = $('#idResponsable').text();
    var nombre = $('#nombre').val();
    var objetivo = $('#objetivo').val();
    var tipo = $('#tipo').val();
    var convocatoria = $("input[name='convocatoria']:checked").val();
    var listSesiones = sesiones();
    var listColab = colaboradores();                  
    var resultado =  $('#summernote').summernote('code');
    resultado = resultado.replace(/"/g, "'");          
    var listBenef;
    var volumen;
    if( tipoConvocatoria() == 'Especifica'){
      listBenef = beneficiarios();  
      volumen = listBenef.length;
    }else if(  tipoConvocatoria() == 'Masiva'){          
      volumen = $('#volumen').val();
    }   
    var obj = '{"id":"'+id+'", "nombre":"'+nombre+'", "objetivo":"'+objetivo+'",  "tipo":"'+tipo+'", "convocatoria":"'+convocatoria+'", "idResponsable":"'+idResponsable+'", "volumen":"'+volumen+'", "resultado":"'+resultado+'", "creador":"'+idResponsable+'", "sesiones": [';
    for (var i = 0; i < listSesiones.length; i++) {
      if((i+1) == listSesiones.length){
        obj += '{ "numero":"'+listSesiones[i].numero+'", "nombre":"'+listSesiones[i].nombre+'", "descripcion":"'+listSesiones[i].descripcion+'","lugar":"'+listSesiones[i].lugar+'", "fechaHora":"'+listSesiones[i].fechaHora+'", "duracion":"'+listSesiones[i].duracion+'"}';  
      }else{          
        obj += '{ "numero":"'+listSesiones[i].numero+'", "nombre":"'+listSesiones[i].nombre+'", "descripcion":"'+listSesiones[i].descripcion+'","lugar":"'+listSesiones[i].lugar+'", "fechaHora":"'+listSesiones[i].fechaHora+'", "duracion":"'+listSesiones[i].duracion+'"},';  
      }      
    }

    if( tipoConvocatoria() == 'Especifica'){
      obj += '], "beneficiarios": [';
      for (var i = 0; i < listBenef.length; i++) {
        if((i+1) == listBenef.length){
          obj += '{ "numero":"'+listBenef[i].numero+'", "nombre":"'+listBenef[i].nombre+'", "apellido":"'+listBenef[i].apellido+'", "tipoIdentificacion":"'+listBenef[i].tipoIdentificacion+'", "identificacion":"'+listBenef[i].identificacion+'", "sexo":"'+listBenef[i].sexo+'", "edad":"'+listBenef[i].edad+'"}';  
        }else{
          obj += '{ "numero":"'+listBenef[i].numero+'", "nombre":"'+listBenef[i].nombre+'", "apellido":"'+listBenef[i].apellido+'", "tipoIdentificacion":"'+listBenef[i].tipoIdentificacion+'", "identificacion":"'+listBenef[i].identificacion+'", "sexo":"'+listBenef[i].sexo+'", "edad":"'+listBenef[i].edad+'"},';  
        }      
      }
    }


    obj += '], "colaboradores": [';
    for (var i = 0; i < listColab.length; i++) {
      if((i+1) == listColab.length){
        obj += '{ "id":"'+listColab[i].id+'", "nombre":"'+listColab[i].nombre+'", "apellido":"'+listColab[i].apellido+'", "tipoIdentificacion":"'+listColab[i].tipoIdentificacion+'", "identificacion":"'+listColab[i].identificacion+'", "email":"'+listColab[i].email+'", "tipo":"'+listColab[i].tipo+'", "idUnidadAcademica":"'+listColab[i].idUnidadAcademica+'", "sexo":"'+listColab[i].sexo+'" }';  
      }else{
        obj += '{ "id":"'+listColab[i].id+'", "nombre":"'+listColab[i].nombre+'", "apellido":"'+listColab[i].apellido+'", "tipoIdentificacion":"'+listColab[i].tipoIdentificacion+'", "identificacion":"'+listColab[i].identificacion+'", "email":"'+listColab[i].email+'", "tipo":"'+listColab[i].tipo+'", "idUnidadAcademica":"'+listColab[i].idUnidadAcademica+'", "sexo":"'+listColab[i].sexo+'" },';  
      }      
    }    
    obj += ']}';
         
    $.post(
      "Controllers/AjaxController.php?funcion=editarActividad",            
      {
          json : obj                
      },
      function(data,status){              
        if(data != 0){
          swal({                
            title: "Actividad Editada",
            text: "La actividad ha sido editada exitosamente, los detalles se mostraran a continuacion ",
            type: "success"
          }, function(){
            window.location.href = 'actividad.php?idActividad='+data;  
          }); 
          
        }else{
          swal({                
            title: "Error",
            text: "Ha ocurrido un error al editar la actividad",
            type: "error"
          }); 
        }
      }
    );  
  }

  function beneficiarios(){    
    var list = new Array();
    var benef;
    $('.benef').each(function() {      
        sesion = { 
          "numero":$(this).children().eq(0).text(), 
          "nombre":$(this).children().eq(1).text(), 
          "apellido":$(this).children().eq(2).text(), 
          "tipoIdentificacion":$(this).children().eq(3).text(),
          "identificacion":$(this).children().eq(4).text(),
          "sexo":( ( $(this).children().eq(5).text() == 'Masculino' ) ? 'M' : 'F'),
          "edad":$(this).children().eq(6).text()
        }
        list.push(sesion);                    
    }); 
    return list;
  }

  function dateTimeFormat(date, time){        
    var age = date.substring(6, 10);
    var month = date.substring(0, 2);   
    var day =  date.substring(3, 5);
     
    var hour = time.substring(0, 3);
    var minute = time.substring(4, 6);        
    var dateTime = age+'-'+month+'-'+day+' '+hour+':'+minute;     
    return dateTime;     
  }

  function dateFormat(date){
     var age = date.substring(6, 10);
     var month = date.substring(0, 2);   
     var day =  date.substring(3, 5);

     var dateFormat = age+'-'+month+'-'+day;
    return dateFormat;     
  }  
  

  function UnDateFormat(date){        
    var age = date.substring(0, 4);
    var month = date.substring(5, 7);   
    var day =  date.substring(8, 10);

    var dateFormat = month+'/'+day+'/'+age;
    return dateFormat;     
  }   

//=========================================================================================================================================================//
//==================================================================NUEVO DESARROLLO=======================================================================//
//=========================================================================================================================================================//

  function unidadesAcademicas(tipo){    
    $.post(
      "Controllers/AjaxController.php?funcion=unidadesAcademicasTipo",            
      {
          tipo: tipo          
      },
      function(data,status){                 
        if(data != 0){
          var obj = JSON.parse(data);
          var option = '';
          for (i in obj){
            option += '<option value="'+obj[i]['id']+'">'+obj[i]['nombre']+'</option>';        
          }        
          $('#unidadAcademicaCol').append(option);
        }                           
      }
    );
  }

  unidadesAcademicas('Programa');

  $('#identificacionCol').change(function(){
    buscarColaborador();
  });

  $('#tipoIdentCol').change(function(){
    buscarColaborador();
  });

  function buscarColaborador(){
    var tipoIdent = $('#tipoIdentCol').val();
    var identificacion = $('#identificacionCol').val();
    var permiso = 'Colaborador';
    $('.inputHidenCol').fadeIn();    
    $.post(
      "Controllers/AjaxController.php?funcion=consultarColaboador",            
      {
          tipoIdent : tipoIdent,
          identificacion: identificacion,
          permiso : permiso       
      },
      function(data,status){        
        if(data != 0){
           var obj = JSON.parse(data);
           $('#nombreCol').val(obj.nombre);
           $('#apellidoCol').val(obj.apellido);
           $('#sexoCol').val(obj.sexo);
           $('#emailCol').val(obj.email);
           $('#tipoCol').val(obj.tipo);           
           $('#unidadAcademicaCol').val(obj.unidadAcademica);      
           $('#idCol').text(obj.id);
           $('#add-colab').text('');
           $('#add-colab').append('<i class="fa fa-plus"></i> Agregar');
           $('.inputCol').attr('disabled', 'disabled');     
        }else{
           setDefaultInputsColab();
        }
      }
    );
  }

  function colaboradores(){    
    var list = new Array();
    var colab;
    $('.colab').each(function(){      
        colab = { 
          "nombre":$(this).children().eq(1).text(), 
          "apellido":$(this).children().eq(2).text(), 
          "tipoIdentificacion":$(this).children().eq(3).text(), 
          "identificacion":$(this).children().eq(4).text(), 
          "email":$(this).children().eq(5).text(), 
          "tipo":$(this).children().eq(6).text(), 
          "idUnidadAcademica":$(this).children().eq(7).children().first().text(), 
          "sexo":( ( $(this).children().eq(8).text() == 'Masculino' ) ? 'M' : 'F'),                                   
          "id":$(this).children().eq(9).text()
        }        

        list.push(colab);                    
    });     
    return list;  
  }

  $('.prueba').click(function(){    
    colaboradores();
  });

  function sesiones(){    
    var list = new Array();
    var sesion;
    $('.sesion').each(function() {      
        sesion = { 
          "numero":$(this).children().eq(0).text(), 
          "nombre":$(this).children().eq(1).text(), 
          "descripcion":$(this).children().eq(2).text(), 
          "lugar":$(this).children().eq(3).text(), 
          "fechaHora": dateTimeFormat( $(this).children().eq(4).text() ,  $(this).children().eq(5).text() ),          
          "duracion":$(this).children().eq(5).text()         
        }      
        list.push(sesion);                    
    }); 
    return list;
  }


  function setDefaultInputsColab(){
    $('#nombreCol').val('');
    $('#apellidoCol').val('');    
    $('#emailCol').val('');      
    $('#add-colab').text('');  
    $('#idCol').text('');  
    $('#add-colab').append('<i class="fa fa-plus"></i> Crear');
    $('.inputCol').removeAttr('disabled'); 
  }

  // $('#add-colab').click(function(){
  //   $('.alert-dismissible').remove();
    
  //     var tipoIdent = $('#tipoIdentCol option:selected').text();
  //     var identificacion = $('#identificacionCol').val();
  //     var nombre = $('#nombreCol').val();
  //     var apellido = $('#apellidoCol').val();
  //     var sexo = $('#sexoCol option:selected').text();
  //     var email = $('#emailCol').val();
  //     var tipo = $("#tipoCol option:selected").text();
  //     var idUnidad = $("#unidadAcademicaCol option:selected").val();
  //     var unidad = $("#unidadAcademicaCol option:selected").text();
  //     var id = $('#idCol').text();

  //     var num = countElementsTable('.colab');
  //     var parid;
  //     if((num % 2) == 0){
  //       parid = 'odd';
  //     }else{
  //       parid = 'even';
  //     }

  //     if(inputTextValidation('.inputCol')){
  //       if(!findDuplicateColab(identificacion) ){      
  //         var colaborador = '<tr class="'+parid+' pointer colab"> <td>'+(num+1)+'</td> <td>'+nombre+'</td> <td>'+apellido+'</td> <td>'+tipoIdent+'</td> <td class="identRow">'+identificacion+'</td> <td>'+email+'</td> <td>'+tipo+'</td> <td><span hidden>'+idUnidad+'</span><span>'+unidad+'</span></td> <td>'+sexo+'</td> <td class="none">'+id+'</td> <td></i> <i class="fa fa-remove close-colab"></i></td> <input type="hidden" value="'+id+'" class="idColab"> </tr>';
  //         $('#table-colab').children().next().append(colaborador);
  //         setDefaultInputsColab();
  //         $('#identificacionCol').val('');
  //       }else{
  //         swal({                
  //             title: "Alerta",
  //             text: "El colaborador ya a sido agregado",
  //             type: "info"
  //           });
  //       }
  //     }
  // });

  function inputTextValidation(validacionTab){
    var val = true;    
    $('.text-danger').remove();
    $(validacionTab).each(function(){
      var valor = $(this).val();
      if(valor == ''){      
        $(this).after('<p class="text-danger">Este campo es obligatorio</p>');
        val = false;
      }      
    });
    return val;
  }


  function countElementsTable(table){
    var count = 0;
    $(table).each(function(){
      count++;
    });
    return count;     
  }

  $(".list-colab").on("click", ".close-colab", function(){            
      $(this).parent().parent().remove();  
      reasignNumberItems('.colab');    
  });


  function findDuplicateColab(ident){    
    var exist = false;
    $('.identRow').each(function(){      
      if(new String(ident).valueOf() == new String($(this).text()).valueOf()){        
        exist = true;
        }        
      }
    );
    return exist;
  }

  $('.inputCol').change(function(){
    $(this).next().remove();
  })

  $('#editar-parc').click(function(){
    if(tableEmpty('.colab', '#table-colab')){    
      editarActividad();
    }
  });

//=========================================================================================================================================================//
//==================================================================SEGUNDO NUEVO DESARROLLO=======================================================================//
//=========================================================================================================================================================//

  $('#tipo-documento').change(function(){
    buscarBeneficiario();
  });

  $('#documento').change(function(){
    buscarBeneficiario();
  });

  function buscarBeneficiario(){
    var tipoIdent = $('#tipo-documento').val();
    var identificacion = $('#documento').val();    
    $('.inputHidenBenef').fadeIn();    
    $.post(
      "Controllers/AjaxController.php?funcion=consultarBeneficiario",            
      {
          tipoIdent : tipoIdent,
          identificacion: identificacion          
      },
      function(data,status){        
        if(data != 0){
           var obj = JSON.parse(data);
           $('#nombre-beneficiario').val(obj.nombre);
           $('#apelido-beneficiario').val(obj.apellido);
           $('#sexo-beneficiario').val(obj.sexo);
           $('#edad-beneficiario').val(obj.edad);         
           $('#idBenef').text(obj.id);
           $('#add-benef').text('');
           $('#add-benef').append('<i class="fa fa-plus"></i> Agregar');
           $('.inputBenef').attr('disabled', 'disabled');     
        }else{
           setDefaultInputsBenef();
        }
      }
    );
  }

  function setDefaultInputsBenef(){
    $('#nombre-beneficiario').val('');
    $('#apelido-beneficiario').val('');       
    $('#add-benef').text('');  
    $('#idBenef').text('');  
    $('#add-benef').append('<i class="fa fa-plus"></i> Crear');
    $('.inputBenef').removeAttr('disabled'); 
  }

});