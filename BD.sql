-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-11-2017 a las 23:41:28
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `psu`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activiadades_beneficiarios`
--

CREATE TABLE `activiadades_beneficiarios` (
  `id_actividades` int(11) NOT NULL,
  `id_beneficiarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades`
--

CREATE TABLE `actividades` (
  `id` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `objetivo` varchar(1200) NOT NULL,
  `resultado` varchar(20000) DEFAULT NULL,
  `volumen` int(11) NOT NULL,
  `convocatoria` varchar(50) NOT NULL,
  `id_actividad` int(11) DEFAULT NULL,
  `id_colaborador` int(11) DEFAULT NULL,
  `id_creador` int(11) DEFAULT NULL,
  `tipo` varchar(50) NOT NULL,
  `activo` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades_colaboradores`
--

CREATE TABLE `actividades_colaboradores` (
  `id_actividades` int(11) NOT NULL,
  `id_colaboradores` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `beneficiarios`
--

CREATE TABLE `beneficiarios` (
  `id` int(11) NOT NULL,
  `tipo_identificacion` varchar(50) DEFAULT NULL,
  `identificacion` varchar(50) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) NOT NULL,
  `edad` int(11) NOT NULL,
  `sexo` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colaboradores`
--

CREATE TABLE `colaboradores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(60) NOT NULL,
  `sexo` varchar(5) NOT NULL,
  `email` varchar(60) NOT NULL,
  `tipo_identificacion` varchar(50) NOT NULL,
  `identificacion` varchar(50) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `clave` varchar(60) NOT NULL,
  `permiso` varchar(20) NOT NULL,
  `activo` int(5) NOT NULL,
  `id_unidad_academica` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `colaboradores`
--

INSERT INTO `colaboradores` (`id`, `nombre`, `apellido`, `sexo`, `email`, `tipo_identificacion`, `identificacion`, `tipo`, `clave`, `permiso`, `activo`, `id_unidad_academica`) VALUES
(2, 'Andres', 'Castañeda', 'M', 'lidier.castaneda@javeriana.edu.co', 'Cedula de Ciudadania', '1019086911', 'Administrativo', 'd55d7da44aac6212c19d41b128a1de10', 'Administrador', 1, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesiones`
--

CREATE TABLE `sesiones` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(1200) DEFAULT NULL,
  `lugar` varchar(150) NOT NULL,
  `fechaHora` datetime DEFAULT NULL,
  `duracion` varchar(50) NOT NULL,
  `id_actividad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidadesacademicas`
--

CREATE TABLE `unidadesacademicas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 NOT NULL,
  `tipo` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `id_unidad_academica` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `unidadesacademicas`
--

INSERT INTO `unidadesacademicas` (`id`, `nombre`, `tipo`, `id_unidad_academica`) VALUES
(1, 'Arquitectura y Diseno', 'Facultad', NULL),
(2, 'Artes', 'Facultad', NULL),
(3, 'Estetica', 'Facultad', NULL),
(4, 'Ciencias', 'Facultad', NULL),
(5, 'Ciencias economicas y Administrativas', 'Facultad', NULL),
(6, 'Ciencias Politicas y Relaciones Internacionales', 'Facultad', NULL),
(7, 'Ciencias Sociales', 'Facultad', NULL),
(8, 'Comunicacion y Lenguaje', 'Facultad', NULL),
(9, 'Derecho Canonico', 'Facultad', NULL),
(10, 'Educacion', 'Facultad', NULL),
(11, 'Enfermeria', 'Facultad', NULL),
(12, 'Estudios Ambientales y Rurales', 'Facultad', NULL),
(13, 'Filosofia', 'Facultad', NULL),
(14, 'Ingenieria', 'Facultad', NULL),
(15, 'Medicina', 'Facultad', NULL),
(16, 'Odontologia', 'Facultad', NULL),
(17, 'Psicologia', 'Facultad', NULL),
(18, 'Teologia', 'Facultad', NULL),
(19, 'Arquitectura', 'Departamento', NULL),
(20, 'Diseno ', 'Departamento', NULL),
(21, 'Estetica', 'Departamento', NULL),
(22, 'Artes Escenicas', 'Departamento', NULL),
(23, 'Artes Visuales', 'Departamento', NULL),
(24, 'Musica', 'Departamento', NULL),
(25, 'Biologia', 'Departamento', NULL),
(26, 'Fisica', 'Departamento', NULL),
(27, 'Matematicas', 'Departamento', NULL),
(28, 'Microbiologia', 'Departamento', NULL),
(29, 'Nutricion y Bioquimica', 'Departamento', NULL),
(30, 'Quimica', 'Departamento', NULL),
(31, 'Administracion', 'Departamento', NULL),
(32, 'Ciencias Contables', 'Departamento', NULL),
(33, 'Economia', 'Departamento', NULL),
(34, 'Derecho economico', 'Departamento', NULL),
(35, 'Derecho Laboral', 'Departamento', NULL),
(36, 'Derecho Penal', 'Departamento', NULL),
(37, 'Derecho Privado', 'Departamento', NULL),
(38, 'Derecho Procesal', 'Departamento', NULL),
(39, 'Derecho Publico', 'Departamento', NULL),
(40, 'Filosofia e Historia del Derecho', 'Departamento', NULL),
(41, 'Ciencia Politica', 'Departamento', NULL),
(42, 'Relaciones Internacionales', 'Departamento', NULL),
(43, 'Antropologia', 'Departamento', NULL),
(44, 'Estudios Culturales', 'Departamento', NULL),
(45, 'Historia', 'Departamento', NULL),
(46, 'Literatura', 'Departamento', NULL),
(47, 'Sociologia', 'Departamento', NULL),
(48, 'Ciencia de la Informacion', 'Departamento', NULL),
(49, 'Comunicacion', 'Departamento', NULL),
(50, 'Lenguas', 'Departamento', NULL),
(51, 'Formacion', 'Departamento', NULL),
(52, 'Enfermeria en salud Colectiva', 'Departamento', NULL),
(53, 'Enfermeria Clinica', 'Departamento', NULL),
(54, 'Desarrollo Rural y Regional', 'Departamento', NULL),
(55, 'Ecologia y Territorio', 'Departamento', NULL),
(56, 'Filosofia', 'Departamento', NULL),
(57, 'Civil', 'Departamento', NULL),
(58, 'Arquitectura', 'Programa', NULL),
(59, 'Diseno Industrial', 'Programa', NULL),
(60, 'Esp. Diseno y Gerencia de Producto', 'Programa', NULL),
(61, 'Artes Escenicas', 'Programa', NULL),
(62, 'Artes Visuales', 'Programa', NULL),
(63, 'Estudios Musicales', 'Programa', NULL),
(64, 'Biologia', 'Programa', NULL),
(65, 'Bacteriologia', 'Programa', NULL),
(66, 'Nutricion y Dietetica', 'Programa', NULL),
(67, 'Microbiologia Industrial', 'Programa', NULL),
(68, 'Matematicas', 'Programa', NULL),
(69, 'Administracion de Empresas', 'Programa', NULL),
(70, 'Contaduria Publica', 'Programa', NULL),
(71, 'Economia', 'Programa', NULL),
(72, 'Derecho', 'Programa', NULL),
(73, 'Ciencia Politica', 'Programa', NULL),
(74, 'Relaciones Internacionales', 'Programa', NULL),
(75, 'Antropologia', 'Programa', NULL),
(76, 'Historia', 'Programa', NULL),
(77, 'Estudios Literarios', 'Programa', NULL),
(78, 'Sociologia', 'Programa', NULL),
(79, 'Ciencia de la Informacion - Bibliotecologia', 'Programa', NULL),
(80, 'Comunicacion Social', 'Programa', NULL),
(81, 'Lic. en Lenguas', 'Programa', NULL),
(82, 'Lic. En Filosofia', 'Programa', NULL),
(83, 'Lic. En educacion Infantil', 'Programa', NULL),
(84, 'Enfermeria', 'Programa', NULL),
(85, 'Ecologia', 'Programa', NULL),
(86, 'Filosofia', 'Programa', NULL),
(87, 'Ingenieria Civil', 'Programa', NULL),
(88, 'Ingenieria Electronica', 'Programa', NULL),
(89, 'Ingenieria Industrial', 'Programa', NULL),
(90, 'Ingenieria de Sistemas', 'Programa', NULL),
(91, 'Medicina', 'Programa', NULL),
(92, 'Esp. En Geriatria', 'Programa', NULL),
(93, 'Esp. En Pediatria', 'Programa', NULL),
(94, 'Odontologi', 'Programa', NULL),
(95, 'Psicologia', 'Programa', NULL),
(96, 'Teologia', 'Programa', NULL),
(97, 'Lic. En Teologia', 'Programa', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activiadades_beneficiarios`
--
ALTER TABLE `activiadades_beneficiarios`
  ADD PRIMARY KEY (`id_actividades`,`id_beneficiarios`),
  ADD KEY `IXFK_Activiadades_Beneficiarios_Actividades` (`id_actividades`),
  ADD KEY `IXFK_Activiadades_Beneficiarios_Beneficiarios` (`id_beneficiarios`);

--
-- Indices de la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_Actividades_Actividades` (`id_actividad`),
  ADD KEY `IXFK_Actividades_Colaboradores` (`id_colaborador`),
  ADD KEY `IXFK_Actividades_Colaboradores_Creador` (`id_creador`);

--
-- Indices de la tabla `actividades_colaboradores`
--
ALTER TABLE `actividades_colaboradores`
  ADD PRIMARY KEY (`id_actividades`,`id_colaboradores`),
  ADD KEY `IXFK_Actividades_Beneficiarios_Actividades` (`id_actividades`),
  ADD KEY `IXFK_Actividades_Beneficiarios_Colaboradores` (`id_colaboradores`);

--
-- Indices de la tabla `beneficiarios`
--
ALTER TABLE `beneficiarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `colaboradores`
--
ALTER TABLE `colaboradores`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `identificacion` (`identificacion`),
  ADD KEY `IXFK_Colaboradores_UnidadesAcademicas` (`id_unidad_academica`);

--
-- Indices de la tabla `sesiones`
--
ALTER TABLE `sesiones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_Sesiones_Actividades` (`id_actividad`);

--
-- Indices de la tabla `unidadesacademicas`
--
ALTER TABLE `unidadesacademicas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_UnidadesAcademicas_UnidadesAcademicas` (`id_unidad_academica`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividades`
--
ALTER TABLE `actividades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT de la tabla `beneficiarios`
--
ALTER TABLE `beneficiarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT de la tabla `colaboradores`
--
ALTER TABLE `colaboradores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `sesiones`
--
ALTER TABLE `sesiones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT de la tabla `unidadesacademicas`
--
ALTER TABLE `unidadesacademicas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `activiadades_beneficiarios`
--
ALTER TABLE `activiadades_beneficiarios`
  ADD CONSTRAINT `FK_Activiadades_Beneficiarios_Actividades` FOREIGN KEY (`id_actividades`) REFERENCES `actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Activiadades_Beneficiarios_Beneficiarios` FOREIGN KEY (`id_beneficiarios`) REFERENCES `beneficiarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD CONSTRAINT `FK_Actividades_Actividades` FOREIGN KEY (`id_actividad`) REFERENCES `actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Actividades_Colaboradores` FOREIGN KEY (`id_colaborador`) REFERENCES `colaboradores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Actividades_Colaboradores_Creador` FOREIGN KEY (`id_creador`) REFERENCES `colaboradores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `actividades_colaboradores`
--
ALTER TABLE `actividades_colaboradores`
  ADD CONSTRAINT `FK_Actividades_Beneficiarios_Actividades` FOREIGN KEY (`id_actividades`) REFERENCES `actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Actividades_Beneficiarios_Colaboradores` FOREIGN KEY (`id_colaboradores`) REFERENCES `colaboradores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `colaboradores`
--
ALTER TABLE `colaboradores`
  ADD CONSTRAINT `FK_Colaboradores_UnidadesAcademicas` FOREIGN KEY (`id_unidad_academica`) REFERENCES `unidadesacademicas` (`id`);

--
-- Filtros para la tabla `sesiones`
--
ALTER TABLE `sesiones`
  ADD CONSTRAINT `FK_Sesiones_Actividades` FOREIGN KEY (`id_actividad`) REFERENCES `actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `unidadesacademicas`
--
ALTER TABLE `unidadesacademicas`
  ADD CONSTRAINT `FK_UnidadesAcademicas_UnidadesAcademicas` FOREIGN KEY (`id_unidad_academica`) REFERENCES `unidadesacademicas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
