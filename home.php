<!DOCTYPE html>
<html lang="en"></script>
  <head>
    <link rel="shortcut icon" href="favicon.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></script>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8"></script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1"></script>

    <title>Vidas moviles |  Actividades</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="styles/vendors/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="styles/vendors/font-awesome/css/font-awesome.min.css">
    <!-- NProgress -->
    <link rel="stylesheet" href="styles/vendors/nprogress/nprogress.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="styles/vendors/iCheck/skins/flat/green.css">
	
    <!-- bootstrap-progressbar -->
    <link rel="stylesheet" href="styles/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="styles/vendors/jqvmap/dist/jqvmap.min.css">
    <!-- bootstrap-daterangepicker -->
    <link rel="stylesheet" href="styles/vendors/bootstrap-daterangepicker/daterangepicker.css">    
    <!-- Custom Theme Style -->
    <link rel="stylesheet" href="styles/build/css/custom.min.css"></script>
    <!-- bootstrap-daterangepicker -->
    <link href="styles/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- sweetalert -->
    <link href="styles/vendors/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Datatables-->
    <link href="styles/vendors/datatables/datatables.min.css" rel="stylesheet">
    
    <link href="styles/vendors/clockpicker/clockpicker.css" rel="stylesheet">

    <link href="styles/css/ld.css" rel="stylesheet">

    <link href="styles/css/style.css" rel="stylesheet">

  </head>

  <?php 
    session_start();
    $colaborador;
    $session = '';
    $navAdmin = '';
    $disableAct = '';
    $idColaborador = '';
    $script = 'home.js';
    
    if(isset($_SESSION['colaborador'])){
      $colaborador = json_decode($_SESSION['colaborador'],true);            
      $idColaborador = "var idUsuario = ".$colaborador['id'].";";      
      if($colaborador['permiso'] == 'Administrador'){
        $navAdmin = '<li> <a href="colaboradores.php"> <i class="fa fa-hand-paper-o"></i> Responsables</a> </li>';
        $disableAct = '<li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a> <ul class="dropdown-menu" role="menu"> <li><a id="off-activities" href="#">Desactivar actividades</a> </li> </ul></li>';
        $script = 'home-admin.js';
      }
    }else{
      $session = "window.location.href = 'index.php'";                  
    }
  ?>
  <script type="text/javascript">
    <?php echo $session; ?>  
    <?php echo $idColaborador; ?>  
  </script>
  
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;margin-top:25px; margin-bottom:75px">                            
              <img width="100" src="styles/images/imagotipoSloganBlack.png" class="img-responsive center-block" alt="vidas moviles">            
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div style="  padding-top: 25px; padding-right: 10px; padding-bottom: 10px; padding-left: 30px" class="profile_pic">
                <h1 style="color: #fff; opacity: 0.8"><i class="fa fa-user"></i></h1>
              </div>
              <div class="profile_info">
              <br>                
                <h2><?php echo $colaborador['nombre'] ?> <?php echo $colaborador['apellido'] ?></h2>
                <span><?php echo $colaborador['permiso'] ?></span>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->

            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

              <div class="menu_section">                
                <ul class="nav side-menu">
                  <li><a> <i class="fa fa-home"></i> Actividades <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="crear.php">Crear actividad</a></li>
                      <li><a href="home.php">Lista de actividades</a></li>
                      <li><a href="sesiones.php">Calendario</a></li>                                   
                    </ul>
                  </li>      
                  <?php echo $navAdmin; ?>                                            
                </ul>
              </div>


            </div>
            <!-- /sidebar menu -->
            
          </div>
        </div>


        <!-- top navigation -->

        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user"></i>
                    <?php echo $colaborador['nombre'] ?> <?php echo $colaborador['apellido'] ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">                    
                    <li><a id="logout" href="#"><i class="fa fa-sign-out pull-right"></i> Cerrar Session</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
        <div class="row">
          <div class="col-md-12">
            <div class="x_panel">
              <div class="x_title">
                <h2><i class="fa fa-flag"></i> Lista de actividades</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <?php echo $disableAct; ?>
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>                    
                </ul>
                <div class="clearfix">                
                </div>
              </div>      
              <div class="x_content">
                  <div class="row">
                    <div class="col-xs-12">
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div class="tab-pane active" id="home">                          
                          <!--<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">-->                          
                          <table class="table table-striped table-bordered table-hover dataTables-example1" >
                            <thead>
                              <tr>
                                <th>Tema</th> 
                                <th>Objetivo</th> 
                                <th>Tipo Actividad</th> 
                                <th>Tipo Convocatoria</th> 
                                <th>Acciones</th> 
                              </tr>
                            </thead>
                            <tbody  class="table-actividad-creadas">                                                                                                        
                            </tbody>
                          </table>
                        </div>                        
                      </div>
                    </div>   
                  </div>            
              </div>
            </div>
          </div>            
        </div>       

      
      </div>
        <!-- /page content -->        
        
          <!-- footer content -->
          <footer>
            <div class="pull-right"></script>
              Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com"></script>Colorlib</a>
            </div>
            <div class="clearfix"></script></div>
          </footer>
          <!-- /footer content -->
      </div>
    </div>
    
    <!-- jQuery -->
    <script src="styles/vendors/jquery/dist/jquery.min.js"></script>
    
    
    <!--<script src="styles/js/bootstrap.min.js"></script>-->
    <!-- Bootstrap -->
    <script src="styles/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="styles/vendors/fastclick/lib/fastclick.js"></script>
    
    <!-- NProgress -->
    <script src="styles/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="styles/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="styles/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="styles/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="styles/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="styles/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="styles/vendors/Flot/jquery.flot.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.time.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="styles/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="styles/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="styles/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="styles/vendors/DateJS/build/date.js"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="styles/vendors/moment/min/moment.min.js"></script>
    <script src="styles/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="styles/build/js/custom.min.js"></script>
        <!-- bootstrap-daterangepicker -->
    <script src="styles/vendors/moment/min/moment.min.js"></script>
    <script src="styles/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- sweetalert -->
    <script src="styles/vendors/sweetalert/sweetalert.min.js"></script>
    <!-- Da    <!-- <script src="styles/vendors/datatables/datatables.min.js"></script> -->    
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>tatables -->  

      
    <script src="styles/vendors/jszip/dist/jszip.min.js"></script>
    <script src="styles/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="styles/vendors/pdfmake/build/vfs_fonts.js"></script>
  
  
    <script src="styles/vendors/clockpicker/clockpicker.js"></script>     

    <script src="styles/js/<?php echo $script;?>"></script>       

    <script>
        $(document).ready(function(){

            $('.clockpicker').clockpicker();
            $('#logout').click(function(){

              $.get(
                  "Controllers/AjaxController.php?funcion=logout",
                  function(data,status){                                                                              
                    window.location.href = 'index.php';                 
                  }
              );
                             
            });

        });
    </script>
  </body>
</html>
