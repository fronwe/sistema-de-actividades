<!DOCTYPE html>
<html lang="en"></script>
  <head>
    <link rel="shortcut icon" href="favicon.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></script>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8"></script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1"></script>

    <title>Vidas moviles |  Actividades</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="styles/vendors/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="styles/vendors/font-awesome/css/font-awesome.min.css">
    <!-- NProgress -->
    <link rel="stylesheet" href="styles/vendors/nprogress/nprogress.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="styles/vendors/iCheck/skins/flat/green.css">
	
    <!-- bootstrap-progressbar -->
    <link rel="stylesheet" href="styles/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="styles/vendors/jqvmap/dist/jqvmap.min.css">
    <!-- bootstrap-daterangepicker -->
    <link rel="stylesheet" href="styles/vendors/bootstrap-daterangepicker/daterangepicker.css">    
    <!-- Custom Theme Style -->
    <link rel="stylesheet" href="styles/build/css/custom.min.css"></script>
    <!-- bootstrap-daterangepicker -->
    <link href="styles/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- sweetalert -->
    <link href="styles/vendors/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Datatables-->
    <!-- Datatables-->
    <link href="styles/vendors/datatables/datatables.min.css" rel="stylesheet">

    <link href="styles/vendors/clockpicker/clockpicker.css" rel="stylesheet">
    <link rel="stylesheet" href="styles/css/ld.css">

    <link rel="stylesheet" href="styles/css/style.css">

  </head>

  <?php 
    session_start();
    $colaborador;
    $session = '';
    $navAdmin = '';
    $idColaborador = '';
    
    if(isset($_SESSION['colaborador'])){
      $colaborador = json_decode($_SESSION['colaborador'],true);            
      $idColaborador = "var idUsuario = ".$colaborador['id'].";";

      if($colaborador['permiso'] == 'Administrador'){
        $navAdmin = '<li> <a href="colaboradores.php"> <i class="fa fa-hand-paper-o"></i> Responsables</a> </li>';
      }else{
        $session = "window.location.href = 'home.php'";                  
      }
    }else{
      $session = "window.location.href = 'index.php'";                  
    }
  ?>
  <script type="text/javascript">
    <?php echo $session; ?>  
    <?php echo $idColaborador; ?>  
  </script>
  
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;margin-top:25px; margin-bottom:75px">                            
          <img width="100" src="styles/images/imagotipoSloganBlack.png" class="img-responsive center-block" alt="vidas moviles">            
        </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div style="  padding-top: 25px; padding-right: 10px; padding-bottom: 10px; padding-left: 30px" class="profile_pic">
                <h1 style="color: #fff; opacity: 0.8"><i class="fa fa-user"></i></h1>
              </div>
              <div class="profile_info">
                <br>
                <h2><?php echo $colaborador['nombre'] ?> <?php echo $colaborador['apellido'] ?></h2>
                <span><?php echo $colaborador['permiso'] ?></span>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br/>
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">                
                <ul class="nav side-menu">
                  <li><a> <i class="fa fa-home"></i> Actividades <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="crear.php">Crear actividad</a></li>
                      <li><a href="home.php">Lista de actividades</a></li>
                      <li><a href="sesiones.php">Calendario</a></li>                                                         
                    </ul>
                  </li>      
                  <?php echo $navAdmin; ?>                                            
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>


        <!-- top navigation -->

        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user"></i>
                    <?php echo $colaborador['nombre'] ?> <?php echo $colaborador['apellido'] ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">                    
                    <li><a id="logout" href="#"><i class="fa fa-sign-out pull-right"></i> Cerrar Session</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

        <div class="row">
          <div class="col-md-12">
            <div class="x_panel">
              <div class="x_title">
                <h2><i class="fa fa-hand-paper-o"></i> Responsables <small>Inscritos</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix">                
                </div>
              </div>      
              <div class="x_content">
                    <div class="tab-pane active" id="home">                    
                      <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                          <tr>
                            <th>Unidad Academica</th>      
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Email</th>
                            <th>Sexo</th>   
                            <th>Tipo</th>                                                        
                            <th>Tipo identificacion</th>
                            <th>Identificacion</th>                                                                                            
                            <th>Acciones</th>                                
                          </tr>
                        </thead>
                        <tbody  class="table-colaboradores">
                                                                                                    
                        </tbody>
                      </table>
                    </div>            
              </div>
            </div>
            <div style="display: none" class="x_panel panel-crear-colaborador">
              <div class="x_title">
                <h2><i class="fa fa-plus"></i> Agregar <small>Responsables / Administradores </small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix">                
                </div>
              </div>      
              <div class="x_content">                                      
                <div class="row">

                    <div class="col-md-3">
                      <p class="lead">Nombre</p>                            
                      <input type="text" id="nombre" class="form-control input-text" placeholder="Nombre responsable">  
                    </div>

                    <div class="col-md-3">
                      <p class="lead">Apellido</p>                            
                      <input type="email" id="apellido" class="form-control input-text" placeholder="Apellido responsable">  
                    </div>                    

                    <div class="col-md-3">
                      <p class="lead">Tipo</p>
                      <select id="tipo" name="tipo"  class="form-control">                          
                          <option>Profesor</option>
                          <option>Administrativo</option>                            
                      </select>                                   
                    </div> 

                    <div class="col-md-3">
                      <p class="lead">Sexo</p>
                      <select id="sexo" name="sexo"  class="form-control">
                          <option value="M">Masculino</option>
                          <option value="F">Femenino</option>                            
                      </select>                                   
                    </div> 
                </div>
                <br>  
                <div class="row">
                                  
                  <div class="col-md-3">
                    <p class="lead">Unidad Academica</p>                            
                    <select id="unidad" name="unidad"  class="form-control">
                    </select>                                   
                  </div>  

                  <div class="col-md-3">
                    <p class="lead">Email</p>                            
                    <input type="email" id="email" class="form-control input-text" placeholder="Email responsable">  
                  </div>                  

                  <div class="col-md-3">
                    <p class="lead">Tipo Identificacion</p>
                    <select id="tipoIdentificacion" name="tipoIdentificacion" class="form-control">
                      <option>Cedula de Ciudadania</option>
                      <option>Tarjeta de Identidad</option>
                      <option>Pasaporte</option>
                      <option>Cedula de Extranjero</option>                                         
                    </select>
                  </div>

                  <div class="col-md-3">
                    <p class="lead">Identificacion</p>                            
                    <input type="text" id="identificacion" class="form-control input-text" placeholder="Identificacion responsable">                      
                  </div>
                                                                         
                </div>                                                                  
                <hr>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">                    
                      <button style="background: #26B99A; border: 1px solid #169F85; color: #fff" id="crear" class="form-control btn btn-success">Crear</button>  
                    </div>                                              
                  </div>
                  <div class="col-md-4">
                  </div>
                  <div class="col-md-4">                            
                  </div>                  
                </div> 
              </div>
            </div>

            <div style="display: none" class="x_panel panel-editar-colaborador">
              <div class="x_title">
                <h2><i class="fa fa-edit"></i> Editar <small>Responsable</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix">                
                </div>
              </div>      
              <div class="x_content">                                      

                <div class="row">

                    <div class="col-md-3">
                      <p class="lead">Nombre</p>                            
                      <input type="text" id="nNombre" class="form-control input-text-edit" placeholder="Nombre responsable">  
                    </div>

                    <div class="col-md-3">
                      <p class="lead">Apellido</p>                            
                      <input type="email" id="nApellido" class="form-control input-text-edit" placeholder="Apellido responsable">  
                    </div>                    

                    <div class="col-md-3">
                      <p class="lead">Tipo</p>
                      <select id="nTipo" name="tipo"  class="form-control">                          
                          <option>Profesor</option>
                          <option>Administrativo</option>                                                                        
                      </select>                                   
                    </div> 

                    <div class="col-md-3">
                      <p class="lead">Sexo</p>
                      <select id="nSexo" name="sexo"  class="form-control">
                          <option value="M">Masculino</option>
                          <option value="F">Femenino</option>                            
                      </select>                                   
                    </div> 
                
                </div>
                <br>
                <div class="row">

                  <div class="col-md-3">
                    <p class="lead">Unidad Academica</p>                            
                    <select id="nUnidad" name="unidad"  class="form-control">
                    </select>                                   
                  </div>                  

                  <div class="col-md-3">
                    <p class="lead">Email</p>                            
                    <input type="nEmail" id="nEmail" class="form-control input-text-edit" placeholder="Email responsable">  
                  </div>                  
                
                  <div class="col-md-3">
                    <p class="lead">Tipo Identificacion</p>                            
                    <select id="nTipoIdentificacion" name="tipoIdentificacion" class="form-control">
                      <option>Cedula de Ciudadania</option>
                      <option>Tarjeta de Identidad</option>
                      <option>Pasaporte</option>
                      <option>Cedula de Extranjero</option>                                         
                    </select>                     
                  </div>  

                  <div class="col-md-3">
                    <p class="lead">Identificacion</p>                            
                    <input type="text" id="nIdentificacion" class="form-control input-text-edit" placeholder="Identificacion responsable">                      
                  </div>
            
                </div>                                                 
                <br>                
                <hr>
                <div class="row">
                  <div class="col-md-4">
                    <button  id="cancelar" class="form-control btn btn-info">Cancelar</button>
                  </div>
                  <div class="col-md-4">
                  </div>
                  <div class="col-md-4">
                    <button style="background: #26B99A; border: 1px solid #169F85; color: #fff" id="editar" class="form-control btn btn-success">Editar</button>  
                    <input type="hidden" id="id-colaborador" >                                                            
                  </div>                  
                </div>                                                                            
              </div>
            </div>            

          </div>            
        </div>       

      
      </div>
        <!-- /page content -->        
        
          <!-- footer content -->
          <footer>
            <div class="pull-right"></script>
              Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com"></script>Colorlib</a>
            </div>
            <div class="clearfix"></script></div>
          </footer>
          <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="styles/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="styles/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="styles/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="styles/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="styles/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="styles/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="styles/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="styles/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="styles/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="styles/vendors/Flot/jquery.flot.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.time.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="styles/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="styles/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="styles/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="styles/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="styles/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="styles/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="styles/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="styles/vendors/moment/min/moment.min.js"></script>
    <script src="styles/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="styles/build/js/custom.min.js"></script>
        <!-- bootstrap-daterangepicker -->
    <script src="styles/vendors/moment/min/moment.min.js"></script>
    <script src="styles/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- sweetalert -->
    <script src="styles/vendors/sweetalert/sweetalert.min.js"></script>
    <!-- Datatables -->  
    <script src="styles/vendors/dataTables/datatables.min.js"></script>

    <script src="styles/vendors/jszip/dist/jszip.min.js"></script>
    <script src="styles/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="styles/vendors/pdfmake/build/vfs_fonts.js"></script>
  
  
    <script src="styles/vendors/clockpicker/clockpicker.js"></script>     
    <script src="styles/js/colaboradores.js"></script>   
    <script src="styles/js/md5.pack.js"></script>

    <script>
        $(document).ready(function(){

            $('.clockpicker').clockpicker();
            $('#logout').click(function(){

              $.get(
                  "Controllers/AjaxController.php?funcion=logout",
                  function(data,status){                                                                              
                    window.location.href = 'index.php';                 
                  }
              );
                             
            });

        });
    </script>
  </body>
</html>
