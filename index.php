<!DOCTYPE html>
<html>


<head>
    <link rel="shortcut icon" href="favicon.ico" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Vidas moviles |  Actividades</title>

    <link rel="stylesheet" href="styles/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="styles/css/animate.css">
    <link rel="stylesheet" href="styles/css/style.css">
    <link rel="stylesheet" href="styles/css/ld.css">


</head>
    <body style="background-image:url('styles/images/vidasMovilesFondo.png'); background-repeat: no-repeat ;background-attachment:fixed; background-position:center;">
    <div  class="row fill">
        <div class="col-md-8">
        </div>
        <div  class="col-md-4 fill">
            <div class="panel-registro fill">
                <div  id="panel-registro">
                    <img width="180" src="styles/images/imagotipoSlogan.png" class="img-responsive center-block" alt="vidas moviles">
                    <br>                
                    <p style="color: #333">La plataforma para programar sus actividades </p>
                    <p>Inicie sesión.</p>
                    <form class="m-t" role="form" >
                        <div class="form-group">
                            <input id="email" type="email" class="form-control" placeholder="Correo" required="">
                            <p id="obligatorio" style="display: none" class="text-danger">Este campo es obligatorio</p>
                        </div>
                        <div class="form-group">
                            <input id="password" type="password" class="form-control" placeholder="Contraseña" required="">
                            <p style="display: none" class="text-danger">Este campo es obligatorio</p>
                        </div>
                        <div id="alerta" style="display: none" class="alert alert-danger">
                            <a id="close-alert" class="close">x</a>
                            <strong>Error de autenticacion</strong> El email o la contrasea no son correctos
                        </div>
                        <div id="registro" type="submit" class="btn btn-primary block full-width m-b">Entrar</div>
                    </form>                
                </div>
                <div id="panel-cambio" style="display:none"> 
                    <img width="180" src="vidasmoviles/styles/images/imagotipoSlogan.png" class="img-responsive center-block" alt="vidas moviles">
                    <br>
                    <h3 class="text-center">Cambio de contraseña</h3>
                    <p   style="color: #333">Por favor ingrese una contraseña para asignarla a su cuenta</p>
                    <p>Inicie sesión.</p>
                    <form class="m-t" role="form" >
                        <div class="form-group">
                            <input id="clave" type="password" class="form-control" placeholder="Contraseña" required="">
                            <p id="obligatorio" style="display: none" class="text-danger">Este campo es obligatorio</p>
                        </div>
                        <div class="form-group">
                            <input id="clave-repeat" type="password" class="form-control" placeholder="Repita su contraseña" required="">
                            <p style="display: none" class="text-danger">Este campo es obligatorio</p>
                        </div>
                        <div id="alerta-clave" style="display: none" class="alert alert-danger">
                            <a id="close-alert-clave" class="close">x</a>
                            <strong>Error de asignacion</strong> Las contraseñas no coinciden
                        </div>
                        <div id="alerta-reasignacion" style="display: none" class="alert alert-danger">
                            <a id="close-alert-reasignacion" class="close">x</a>
                            <strong>Error de asignacion</strong> El usuario no puede reasingar la contraseña
                        </div>                        
                        <div id="cambio-clave" type="submit" class="btn btn-primary block full-width m-b">Cambiar e ingresar</div>
                    </form>
                </div>
            </div>
        </div>  
    </div> 

    <!-- Mainly scripts -->
    <script src="styles/js/jquery-2.1.1.js"></script>
    <script src="styles/js/bootstrap.min.js"></script>
    <script src="styles/js/registro.js"></script>
    <script src="styles/js/md5.pack.js"></script>    

    <script>
    $(document).ready(function(){

        $(document).keypress(function(e) {
            if(e.which == 13) {
                registro();
            }
        });


        $('#registro').click(function(){
            registro();               
        });

        function registro(){
            var valido = true;

            if( $('#email').val() == '' ){
                valido = false;
                $('#email').next().fadeIn();
            }

            if( $('#password').val() == '' ){
                valido = false;
                $('#password').next().fadeIn();
            }

            if(valido){
                
                var email = $('#email').val();
                var password = $('#password').val();
                var hash = md5(password);
                $.post(
                    "Controllers/AjaxController.php?funcion=login",            
                    {
                        usuario : email,
                        clave : hash 
                    },
                    function(data,status){
                        if(data == 1){
                            window.location.href = 'home.php';
                        }else if(data == 2){
                            $('#panel-registro').fadeOut(function(){
                                $('#panel-cambio').fadeIn();                                
                            });
                        }else{
                            $('#alerta').fadeIn();
                        }
                    }
                );
            } 
        }   

        $("#cambio-clave").click(function(){

            var valido = true;

            if( $('#clave').val() == '' ){
                valido = false;
                $('#email').next().fadeIn();
            }

            if( $('#clave-repeat').val() == '' ){
                valido = false;
                $('#password').next().fadeIn();
            }


            if( $('#clave').val() != $('#clave-repeat').val() ){
                valido = false;
                $('#alerta-clave').fadeIn();
            }

            if(valido){                
                var email = $('#email').val();                
                var identificacion = $('#password').val();
                var clave = $('#clave').val();
                var hash = md5(clave);
                $.post(
                    "Controllers/AjaxController.php?funcion=reasignar",            
                    {                
                        usuario : email,                  
                        identificacion : identificacion,
                        clave : hash
                    },
                    function(data,status){
                        if(data == 1){
                             window.location.href = 'home.php';
                        }else if(data == 0){
                            $('#alerta-reasignacion').fadeIn();
                        }
                    }
                );
            }
        });     

        $('#email').change(function(){
            $(this).next().fadeOut();
        });

        $('#password').change(function(){
            $(this).next().fadeOut();
        });

        $('#close-alert').click(function(){
            $(this).parent().fadeOut(); 
        });

        $('#close-alert-clave').click(function(){
            $(this).parent().fadeOut(); 
        });

        $('#close-alert-reasignacion').click(function(){
            $(this).parent().fadeOut(); 
        });


    });
    </script>

</body>

</html>
