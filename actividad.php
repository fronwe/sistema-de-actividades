<!DOCTYPE html>
<html lang="en"></script>
  <head>
    <link rel="shortcut icon" href="favicon.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></script>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8"></script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1"></script>

    <title>Vidas moviles |  Actividades</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="styles/vendors/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="styles/vendors/font-awesome/css/font-awesome.min.css">
    <!-- NProgress -->
    <link rel="stylesheet" href="styles/vendors/nprogress/nprogress.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="styles/vendors/iCheck/skins/flat/green.css">
	
    <!-- bootstrap-progressbar -->
    <link rel="stylesheet" href="styles/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="styles/vendors/jqvmap/dist/jqvmap.min.css">
    <!-- bootstrap-daterangepicker -->
    <link rel="stylesheet" href="styles/vendors/bootstrap-daterangepicker/daterangepicker.css">    
    <!-- Custom Theme Style -->
    <link rel="stylesheet" href="styles/build/css/custom.min.css"></script>
    <!-- bootstrap-daterangepicker -->
    <link href="styles/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- sweetalert -->
    <link href="styles/vendors/sweetalert/sweetalert.css" rel="stylesheet">

    <link href="styles/vendors/clockpicker/clockpicker.css" rel="stylesheet">

    <link href="styles/css/ld.css" rel="stylesheet">  
  </head>

  <?php 
    session_start();
    $colaborador;
    $session = '';
    $navAdmin = '';
    $actividad = '';
    if(isset($_SESSION['colaborador'])){
      $colaborador = json_decode($_SESSION['colaborador'],true);      
      if($colaborador['permiso'] == 'Administrador'){
        $navAdmin = '<li> <a href="colaboradores.php"> <i class="fa fa-hand-paper-o"></i> Responsables</a> </li>';
      }
    }else{
      $session = "window.location.href = 'login.php'";            
    }

    if(isset($_GET['idActividad'])){
      $idActividad = $_GET['idActividad'];
      $actividad = 'var idActividad = '.$idActividad;

    }else{
      $session = "window.location.href = 'home.php'";            
    }

  ?>
  <script type="text/javascript">
    <?php echo $session; ?>
    <?php echo $actividad; ?>

  </script>
  
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;margin-top:25px; margin-bottom:75px">                            
          <img width="100" src="styles/images/imagotipoSloganBlack.png" class="img-responsive center-block" alt="vidas moviles">            
        </div> 

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div style="  padding-top: 25px; padding-right: 10px; padding-bottom: 10px; padding-left: 30px" class="profile_pic">
                <h1 style="color: #fff; opacity: 0.8"><i class="fa fa-user"></i></h1>
              </div>
              <div class="profile_info">
                <br>
                <h2><?php echo $colaborador['nombre'] ?> <?php echo $colaborador['apellido'] ?></h2>
                <span><?php echo $colaborador['permiso'] ?></span>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />
            <!-- sidebar menu -->

            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

              <div class="menu_section">                
                <ul class="nav side-menu">
                  <li><a> <i class="fa fa-home"></i> Actividades <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="crear.php">Crear actividad</a></li>
                      <li><a href="home.php">Lista de actividades</a></li>
                      <li><a href="sesiones.php">Calendario</a></li>
                    </ul>
                  </li> 
                  <?php echo $navAdmin; ?>    
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->          
          </div>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user"></i>
                    <?php echo $colaborador['nombre'] ?> <?php echo $colaborador['apellido'] ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">                    
                    <li><a id="logout" href="#"><i class="fa fa-sign-out pull-right"></i> Cerrar Session</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
        <div class="row">
          <div class="col-md-12">
            <div class="x_panel">
              <div class="x_title">
                <h2><i class="fa fa-flag"></i> Actividades <small>Ver</small></h2>                
                <div class="clearfix"></div>
              </div>      
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="x_panel">
              <div class="x_title">
                <h2><i class="fa fa-check"></i> Informacion <small>General</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix">                
                </div>
              </div>      
              <div class="x_content">
                 <div class="row">
                  <div class="col-md-3">                  
                    <h4> <span style="background: #73879C" class="label label-default">Nombre</span></h4>                                                
                    <h4> <span style="background: #73879C" class="label label-default">Tipo</span></h4>                
                    <h4> <span style="background: #73879C" class="label label-default">Objetivo</span></h4></h4>
                  </div>
                  <div class="col-md-9">
                    <h4><span id="nombre"></span> </h4>                                                
                    <h4><span id="tipo"></span> </h4>                
                    <h4><span id="objetivo"></span> </h4>                      
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="x_panel">
              <div class="x_title">
                <h2><i class="fa fa-check"></i> Informacion <small>Especifica</small></h2>

                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix">                
                </div>
              </div>      
              <div class="x_content">              
                <div class="row">
                  <div class="col-md-3">                    
                    <h4> <span style="background: #73879C; color: #FFF" class="label label-default">Responsable</span></h4>
                    <h4> <span style="background: #73879C; color: #FFF" class="label label-default">Volumen</span></h4>                       
                    <h4> <span style="background: #73879C; color: #FFF" class="label label-default">Convocatoria</span></h4>                                
                  </div>
                  <div class="col-md-9">
                    <h4><span id="responsable"></span> </h4>
                    <h4><span id="volumen"></span> </h4>                 
                    <h4><span id="convocatoria"></span> </h4>                                
                  </div>
                </div>            
              </div>
            </div>
          </div>                
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="x_panel">
              <div class="x_title">
                <h2><i class="fa fa-check"></i> Resultado <small>Actividad</small></h2>

                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix">                
                </div>
              </div>      
              <div class="x_content">              
                <div class="row">
                  <div class="col-md-12">                    
                    <div id="resultado"></div>
                  </div>                  
                </div>            
              </div>
            </div>
          </div>
        </div>  
        <div class="row">
          <div class="col-md-12">
            <div class="x_panel">
              <div class="x_title">
                <h2><i class="fa fa-clock-o"></i> Sesiones </h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix">                
                </div>
              </div>      
              <div class="x_content">
                <div class="table-responsive">
                  <table id="table-sesiones" class="table table-sesione table-striped jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th class="column-title">#</th>
                        <th class="column-title">Nombre </th>
                        <th class="column-title">Dirigido a</th>
                        <th class="column-title">Fecha </th>                                                                                    
                      </tr>
                    </thead>
                    <tbody class="list-sesiones">                                
                    </tbody>
                  </table>                                
                </div> 
              </div>
            </div>
          </div>        
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="x_panel">
              <div class="x_title">
                <h2><i class="fa fa-hand-paper-o"></i> Colaboradores </h2>

                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix">                
                </div>
              </div>      
              <div class="x_content">
                 <div class="table-responsive">
                  <table id="table-colab" class="table table-sesione table-striped jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th class="column-title">#</th>
                        <th class="column-title">Nombre </th>
                        <th class="column-title">Apellido </th>
                        <th class="column-title">Tipo identificacion </th>
                        <th class="column-title">Identificacion </th>
                        <th class="column-title">Tipo </th>
                        <th class="column-title">Unidad Academica </th>             
                      </tr>
                    </thead>
                    <tbody class="list-colab">                                
                    </tbody>
                  </table>                                
                </div> 
              </div>
            </div>
          </div>        
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="x_panel">
              <div class="x_title">
                <h2><i class="fa fa-users"></i> Beneficiarios</h2>

                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix">                
                </div>
              </div>      
              <div class="x_content">
                 <div class="table-responsive">
                  <table id="table-benef" class="table table-sesione table-striped jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th class="column-title">#</th>
                        <th class="column-title">Nombre </th>
                        <th class="column-title">Apellido </th>
                        <th class="column-title">Tipo identificacion </th>
                        <th class="column-title">Identificacion </th>
                        <th class="column-title">Sexo </th>
                        <th class="column-title">Edad </th>
                        
                      </tr>
                    </thead>
                    <tbody class="list-benef">                                
                    </tbody>
                  </table>                                
                </div> 
              </div>
            </div>
          </div>        
        </div>                


      </div>
        <!-- /page content -->        
        
          <!-- footer content -->
          <footer>
            <div class="pull-right"></script>
              Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com"></script>Colorlib</a>
            </div>
            <div class="clearfix"></script></div>
          </footer>
          <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="styles/vendors/jquery/dist/jquery.min.js"></script>
    <!--<script src="styles/js/bootstrap.min.js"></script>-->
    <!-- Bootstrap -->
    <script src="styles/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="styles/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="styles/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="styles/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="styles/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="styles/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="styles/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="styles/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="styles/vendors/Flot/jquery.flot.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.time.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="styles/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="styles/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="styles/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="styles/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="styles/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="styles/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="styles/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="styles/vendors/moment/min/moment.min.js"></script>
    <script src="styles/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="styles/build/js/custom.min.js"></script>
        <!-- bootstrap-daterangepicker -->
    <script src="styles/vendors/moment/min/moment.min.js"></script>
    <script src="styles/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- sweetalert -->
    <script src="styles/vendors/sweetalert/sweetalert.min.js"></script>

    <script src="styles/vendors/clockpicker/clockpicker.js"></script> 
    <script src="styles/js/ver_actividad.js"></script>

    <script>
        $(document).ready(function(){

            $('.clockpicker').clockpicker();
            $('#logout').click(function(){

              $.get(
                  "Controllers/AjaxController.php?funcion=logout",
                  function(data,status){                                                                              
                    window.location.href = 'index.php';                 
                  }
              );
                             
            });

        });
    </script>
  </body>
</html>