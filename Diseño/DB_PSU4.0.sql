-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 23-11-2017 a las 10:57:53
-- Versión del servidor: 5.6.36-cll-lve
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `psu`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activiadades_beneficiarios`
--

CREATE TABLE IF NOT EXISTS `activiadades_beneficiarios` (
  `id_actividades` int(11) NOT NULL,
  `id_beneficiarios` int(11) NOT NULL,
  PRIMARY KEY (`id_actividades`,`id_beneficiarios`),
  KEY `IXFK_Activiadades_Beneficiarios_Actividades` (`id_actividades`),
  KEY `IXFK_Activiadades_Beneficiarios_Beneficiarios` (`id_beneficiarios`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `activiadades_beneficiarios`
--

INSERT INTO `activiadades_beneficiarios` (`id_actividades`, `id_beneficiarios`) VALUES
(14, 9),
(14, 16),
(14, 28),
(40, 5),
(40, 9),
(40, 15),
(41, 6),
(41, 8),
(41, 46),
(42, 18),
(42, 27),
(42, 45),
(49, 47);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades`
--

CREATE TABLE IF NOT EXISTS `actividades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `objetivo` varchar(1200) NOT NULL,
  `resultado` varchar(20000) DEFAULT NULL,
  `volumen` int(11) NOT NULL,
  `convocatoria` varchar(50) NOT NULL,
  `id_actividad` int(11) DEFAULT NULL,
  `id_colaborador` int(11) DEFAULT NULL,
  `id_creador` int(11) DEFAULT NULL,
  `tipo` varchar(50) NOT NULL,
  `activo` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_Actividades_Actividades` (`id_actividad`),
  KEY `IXFK_Actividades_Colaboradores` (`id_colaborador`),
  KEY `IXFK_Actividades_Colaboradores_Creador` (`id_creador`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=94 ;

--
-- Volcado de datos para la tabla `actividades`
--

INSERT INTO `actividades` (`id`, `nombre`, `objetivo`, `resultado`, `volumen`, `convocatoria`, `id_actividad`, `id_colaborador`, `id_creador`, `tipo`, `activo`) VALUES
(14, 'Declaracion de Renta', 'Llenar los formularios de la declaracion de renta ', '<h3>Resultado declaracion de renta</h3><p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Tempora, natus assumenda autem neque excepturi ut atque, consectetur harum labore veritatis soluta ratione optio iusto corrupti sunt sint eligendi tempore accusantium modi reprehenderit, laudantium deserunt doloremque? Assumenda facere tempore voluptas eos deserunt id tempora illo blanditiis fugit nesciunt, sint rerum quibusdam earum nisi itaque deleniti, vero aspernatur, provident labore voluptate iusto! Perspiciatis amet nisi, dignissimos necessitatibus minus soluta harum est dolores similique saepe iure? Laudantium dolorem et quasi est maiores, culpa corporis asperiores vitae? Nobis similique optio corporis fuga perferendis necessitatibus cupiditate illum. Praesentium ut adipisci repellat amet eligendi necessitatibus est doloremque debitis provident possimus, accusantium ea quibusdam. Numquam itaque amet rerum laudantium repellat eveniet sunt doloribus sint impedit voluptatem reiciendis sapiente ab iure ipsa ipsum, quod iusto, inventore explicabo nihil dignissimos unde enim ipsam cumque! Laboriosam fugit maiores reprehenderit eius iste quisquam odit modi sit, officiis ullam repudiandae molestias ut in veritatis pariatur, quod ab cumque aperiam sunt nihil inventore facilis sapiente voluptatem! Obcaecati ducimus repellendus quibusdam aliquam corporis, quod dolore, fugit voluptatibus sit, saepe quos modi laudantium doloribus facere! Quasi, maiores ad similique porro illum esse tempora reprehenderit eius enim, cum temporibus veritatis. Impedit architecto eligendi hic unde totam.</p>', 3, 'Especifica', NULL, 5, 5, 'Visita domiciliaria', 0),
(40, 'Lavado de manos', 'Los niÃ±os se lavan las manos', '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam similique perspiciatis quaerat a soluta sunt voluptate officiis fugit corporis molestias distinctio nihil illo laborum, maiores sapiente possimus ea hic incidunt dolore sint, facere molestiae harum tempore! Deserunt doloremque tempore velit dolores blanditiis vel ipsa sunt officiis quos quas molestias voluptatibus adipisci esse aliquam eum fugiat exercitationem id magnam nostrum quia, quisquam maiores itaque temporibus hic? Quas dolor blanditiis accusamus? Facilis in quibusdam pariatur, eveniet illum atque molestias quod odio non eius quia provident illo dolore rerum! Adipisci voluptates nesciunt minus rem laboriosam perspiciatis nobis necessitatibus<i> aliquid sapiente </i>saepe ipsa eaque, <u>eligendi ut ratione</u>, corporis quisquam repudiandae, voluptatem cupiditate tenetur magni! Alias dolorem voluptates impedit expedita nulla. Iure quo voluptatum fuga ducimus rem quidem, ea distinctio saepe aliquam illum, cupiditate minus sapiente asperiores iste omnis reiciendis modi nemo molestiae sed porro harum? Ipsa voluptatibus aliquam repellendus dolores beatae ut nostrum nesciunt vel voluptatem velit sed voluptatum distinctio dignissimos deserunt earum molestias, unde, eaque inventore id nulla reprehenderit amet odit praesentium quos! Repellendus nostrum pariatur eligendi inventore ea obcaecati corporis placeat ipsa quos vel aliquid rerum deleniti <b>voluptas possimus</b>, hic molestias quia officiis mollitia officia repellat unde ratione minima eaque nisi! Adipisci</p><p><br></p><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus harum odio dolor beatae incidunt fuga sapiente, repellendus, neque excepturi ipsum atque sed esse tempore perspiciatis voluptatibus. Praesentium debitis quos obcaecati totam consequatur tempore id vel omnis, magnam odio quia aperiam quaerat iure esse pariatur! Veritatis eaque assumenda esse nostrum tenetur enim voluptate illum fugiat veniam autem facere cumque in, voluptas, quasi iste dolorum, inventore recusandae ex incidunt temporibus. Sed facilis aspernatur eaque error ex, accusamus ab modi, voluptatum soluta, asperiores voluptas tempora laboriosam possimus iusto. Dolore dicta ab hic eos rem quos aspernatur culpa magni. Sunt, architecto! Eius, vero voluptas?<br></p>', 3, 'Especifica', NULL, 4, 4, 'Taller', 0),
(41, 'Vacunacion de hijos', 'Que los padres de familia aprendan las vacunaciones de sus hijos', '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam similique perspiciatis quaerat a soluta sunt voluptate officiis fugit corporis molestias distinctio nihil illo laborum, maiores sapiente possimus ea hic incidunt dolore sint, facere molestiae harum tempore! Deserunt doloremque tempore velit dolores blanditiis vel ipsa sunt officiis quos quas molestias voluptatibus adipisci esse aliquam eum fugiat exercitationem id magnam nostrum quia, quisquam maiores itaque temporibus hic? Quas dolor blanditiis accusamus? Facilis in quibusdam pariatur, eveniet illum atque molestias quod odio non eius quia provident illo dolore rerum! Adipisci voluptates nesciunt minus rem laboriosam perspiciatis nobis necessitatibus aliquid sapiente saepe ipsa eaque, eligendi ut ratione, corporis quisquam repudiandae, voluptatem cupiditate tenetur magni! Alias dolorem voluptates impedit expedita nulla. Iure quo voluptatum fuga ducimus rem quidem, ea distinctio saepe aliquam illum, cupiditate minus sapiente asperiores iste omnis reiciendis modi nemo molestiae sed porro harum? Ipsa voluptatibus aliquam repellendus dolores beatae ut nostrum nesciunt vel voluptatem velit sed voluptatum distinctio dignissimos deserunt earum molestias, unde, eaque inventore id nulla reprehenderit amet odit praesentium quos! Repellendus nostrum pariatur eligendi inventore ea obcaecati corporis placeat ipsa quos vel aliquid rerum deleniti voluptas possimus, hic molestias quia officiis mollitia officia repellat unde ratione minima eaque nisi! Adipisci.</p><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus harum odio dolor beatae incidunt fuga sapiente, repellendus, neque excepturi ipsum atque sed esse tempore perspiciatis voluptatibus. Praesentium debitis quos obcaecati totam consequatur tempore id vel omnis, magnam odio quia aperiam quaerat iure esse pariatur! Veritatis eaque assumenda esse nostrum tenetur enim voluptate illum fugiat veniam autem facere cumque in, voluptas, quasi iste dolorum, inventore recusandae ex incidunt temporibus. Sed facilis aspernatur eaque error ex, accusamus ab modi, voluptatum soluta, asperiores voluptas tempora laboriosam possimus iusto. Dolore dicta ab hic eos rem quos aspernatur culpa magni. Sunt, architecto! Eius, vero voluptas?<br></p>', 3, 'Especifica', NULL, 4, 4, 'Charla', 0),
(42, 'Capacitacion Hoja de vida', 'Los asistentes aprendar a hacer su hoja de vida personal', '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus harum odio dolor beatae incidunt fuga sapiente, repellendus, neque excepturi ipsum atque sed esse tempore perspiciatis voluptatibus. Praesentium debitis quos obcaecati totam consequatur tempore id vel omnis, magnam odio quia aperiam quaerat iure esse pariatur! Veritatis eaque assumenda esse nostrum tenetur enim voluptate illum fugiat veniam autem facere cumque in, voluptas, quasi iste dolorum, inventore recusandae ex incidunt temporibus. Sed facilis aspernatur eaque error ex, accusamus ab modi, voluptatum soluta, asperiores voluptas tempora laboriosam possimus iusto. Dolore dicta ab hic eos rem quos aspernatur culpa magni. Sunt, architecto! Eius, vero voluptas?</p><p><br></p><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus harum odio dolor beatae incidunt fuga sapiente, repellendus, neque excepturi ipsum atque sed esse tempore perspiciatis voluptatibus. Praesentium debitis quos obcaecati totam consequatur tempore id vel omnis, magnam odio quia aperiam quaerat iure esse pariatur! Veritatis eaque assumenda esse nostrum tenetur enim voluptate illum fugiat veniam autem facere cumque in, voluptas, quasi iste dolorum, inventore recusandae ex incidunt temporibus. Sed facilis aspernatur eaque error ex, accusamus ab modi, voluptatum soluta, asperiores voluptas tempora laboriosam possimus iusto. Dolore dicta ab hic eos rem quos aspernatur culpa magni. Sunt, architecto! Eius, vero voluptas?<br></p>', 3, 'Especifica', NULL, 2, 2, 'CapacitaciÃ³n', 0),
(44, 'taller estilos de vida', 'Informar a los participantes sobre los estilos de vida', '<p><br></p>', 0, 'Masiva', NULL, 17, 17, 'Taller', 0),
(49, 'CapacitaciÃ³n', 'capacitar al usuario en', '<p><br></p>', 1, 'Especifica', NULL, 2, 2, 'CapacitaciÃ³n', 0),
(64, 'estilos de vida', 'los niÃ±os aprenda a baÃ±arse las manos', '<p>qwdqdwwqdwdqw</p>', 9, 'Masiva', NULL, 2, 2, 'CapacitaciÃ³n', 1),
(91, 'Partido futbol', 'Hacer deporte', '<p><br></p>', 0, 'Masiva', NULL, 2, 2, 'CapacitaciÃ³n', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades_colaboradores`
--

CREATE TABLE IF NOT EXISTS `actividades_colaboradores` (
  `id_actividades` int(11) NOT NULL,
  `id_colaboradores` int(11) NOT NULL,
  PRIMARY KEY (`id_actividades`,`id_colaboradores`),
  KEY `IXFK_Actividades_Beneficiarios_Actividades` (`id_actividades`),
  KEY `IXFK_Actividades_Beneficiarios_Colaboradores` (`id_colaboradores`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `actividades_colaboradores`
--

INSERT INTO `actividades_colaboradores` (`id_actividades`, `id_colaboradores`) VALUES
(14, 8),
(14, 12),
(40, 10),
(40, 12),
(41, 10),
(41, 11),
(41, 13),
(42, 8),
(42, 9),
(42, 10),
(44, 19),
(44, 20),
(49, 18),
(64, 11),
(64, 19),
(91, 47);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `beneficiarios`
--

CREATE TABLE IF NOT EXISTS `beneficiarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_identificacion` varchar(50) DEFAULT NULL,
  `identificacion` varchar(50) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) NOT NULL,
  `edad` int(11) NOT NULL,
  `sexo` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Volcado de datos para la tabla `beneficiarios`
--

INSERT INTO `beneficiarios` (`id`, `tipo_identificacion`, `identificacion`, `nombre`, `apellido`, `edad`, `sexo`) VALUES
(1, 'Cedula de Ciudadania', '7890187', 'Luis', 'Castillo', 45, 'M'),
(2, 'Cedula de Ciudadania', '234123', 'Juan Camilo', 'FLorez', 34, 'M'),
(3, 'Cedula de Ciudadania', '765345', 'David', 'Suarez', 24, 'M'),
(4, 'Cedula de Ciudadania', '3567678', 'Andrea', 'Camacho', 35, 'F'),
(5, 'Cedula de Ciudadania', '7879554', 'Maria', 'Manrrique', 37, 'F'),
(6, 'Cedula de Ciudadania', '2345245', 'Diego', 'Sanchez', 23, 'M'),
(7, 'Cedula de Ciudadania', '876667', 'Fabian', 'Uribe', 38, 'M'),
(8, 'Cedula de Ciudadania', '1616745', 'Iad', 'Sevilla', 29, 'M'),
(9, 'Cedula de Ciudadania', '1234654', 'Sergio', 'Plazas', 57, 'M'),
(10, 'Cedula de Ciudadania', '987635', 'Carolina', 'Arteaga', 78, 'F'),
(11, 'Cedula de Ciudadania', '5234343', 'Catalina', 'Abella', 45, 'F'),
(12, 'Cedula de Ciudadania', '87646', 'Cesar', 'Lara', 78, 'M'),
(13, 'Cedula de Ciudadania', '9987567', 'David ', 'Acosta', 44, 'M'),
(14, 'Cedula de Ciudadania', '16545636', 'Maria ', 'Fernanda', 33, 'F'),
(15, 'Cedula de Ciudadania', '385617', 'Luna ', 'Gonzales', 23, 'F'),
(16, 'Cedula de Ciudadania', '4567810', 'Luis', 'Castillo', 24, 'M'),
(17, 'Cedula de Ciudadania', '678910199', 'Tatiana ', 'Preciado', 25, 'F'),
(18, 'Cedula de Ciudadania', '123456', 'Moices ', 'Aaguabara', 12, 'M'),
(27, 'Cedula de Ciudadania', '51664989', 'Martha ', 'Rojas Diaz', 12, 'F'),
(28, 'Cedula de Ciudadania', '19398214', 'Carlos ', 'Abella', 34, 'M'),
(29, 'Cedula de Ciudadania', '000000', 'Jhon ', 'Camacho', 35, 'M'),
(30, 'Cedula de Ciudadania', '1902099101', 'Camilo ', 'Bernal', 24, 'M'),
(31, 'Pasaporte', '678988879', 'Juan Camilo ', 'Abella', 25, 'M'),
(32, 'Cedula de Ciudadania', '90000090', 'Camilo ', 'Bernal', 45, 'M'),
(33, 'Cedula de Ciudadania', '1000090', 'Jesus ', 'Casas', 34, 'M'),
(34, 'Cedula de Ciudadania', '6787', 'mono', 'abella', 20, 'M'),
(35, 'Cedula de Ciudadania', '2029019', 'Julian', 'Diaz', 23, 'M'),
(36, 'Cedula de Ciudadania', '5345325', 'Sebastian', 'Castro', 12, 'M'),
(37, 'Cedula de Ciudadania', '643643563', 'Juliana', 'Castillo', 32, 'F'),
(38, 'Cedula de Ciudadania', '24532346', 'Julio', 'Sanchez', 24, 'M'),
(39, 'Cedula de Ciudadania', '8558885', 'Bernal', 'undefined', 35, 'M'),
(40, 'Cedula de Ciudadania', '909090', 'Prueba', 'Unitaria', 26, 'M'),
(41, 'Cedula de extranjero', '1902910999', 'Mirian', 'Buitrago', 30, 'F'),
(42, 'Cedula de extranjero', '12789201', 'Cesar', 'Guerrero', 19, 'M'),
(43, 'Cedula de Ciudadania', '102981771', 'Camilo', 'Diaz', 38, 'M'),
(44, 'Tarjeta de identidad', '90807060', 'Andres', 'PeÃ±ueal', 34, 'M'),
(45, 'Cedula de Ciudadania', '378283772', 'Martin', 'PeÃ±a', 34, 'M'),
(46, 'Cedula de Ciudadania', '3338373', 'Susana', 'Buitrago', 42, 'F'),
(47, 'Cedula de Ciudadania', '1022972086', 'AndrÃ©s ', 'CastaÃ±eda', 25, 'M'),
(48, 'Cedula de Ciudadania', '7777', 'nnini', 'nuinuin', 43, 'M');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colaboradores`
--

CREATE TABLE IF NOT EXISTS `colaboradores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(60) NOT NULL,
  `sexo` varchar(5) NOT NULL,
  `email` varchar(60) NOT NULL,
  `tipo_identificacion` varchar(50) NOT NULL,
  `identificacion` varchar(50) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `clave` varchar(60) NOT NULL,
  `permiso` varchar(20) NOT NULL,
  `activo` int(5) NOT NULL,
  `id_unidad_academica` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `identificacion` (`identificacion`),
  KEY `IXFK_Colaboradores_UnidadesAcademicas` (`id_unidad_academica`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Volcado de datos para la tabla `colaboradores`
--

INSERT INTO `colaboradores` (`id`, `nombre`, `apellido`, `sexo`, `email`, `tipo_identificacion`, `identificacion`, `tipo`, `clave`, `permiso`, `activo`, `id_unidad_academica`) VALUES
(2, 'Sergio', 'Abella', 'M', 'sergioabella63@gmail.com', 'Cedula de Ciudadania', '1019086911', 'Administrativo', 'd55d7da44aac6212c19d41b128a1de10', 'Administrador', 1, 14),
(3, 'Juan', 'Calle', 'M', 'juanca@gmail.com', 'Cedula de Ciudadania', '10293800', 'Profesor', '10293800', 'Responsable', 0, 19),
(4, 'Daniel', 'Rojas', 'M', 'danielkb@hotmail.com', 'Cedula de Ciudadania', '29018999', 'Profesor', 'ef8b4fdb2b4b6722809338be8d3845d8', 'Responsable', 1, 57),
(5, 'Daniella', 'Herrera', 'F', 'daniellaherrera28@hotmail.com', 'Cedula de Ciudadania', '1018029019', 'Profesor', '3f06a7247d5c04d0618552dd3276ac40', 'Responsable', 1, 27),
(6, 'Felipe', 'Pedraza', 'M', 'ejemplo@gmail.com', 'Cedula de Ciudadania', '90809080', 'Administrativo', '90809080', 'Responsable', 1, 31),
(8, 'Rafael', 'Ferrero', 'M', 'rafa@gmail.com', 'Cedula de Ciudadania', '89908990', 'Estudiante', 'default', 'Colaborador', 1, 90),
(9, 'Julian', 'Barreto', 'M', 'julianb@gmail.com', 'Cedula de Ciudadania', '78897889', 'Estudiante', 'default', 'Colaborador', 1, 87),
(10, 'Valeria', 'Ortega', 'F', 'valeriaorti@hotmail.com', 'Cedula de Ciudadania', '29289279', 'Estudiante', 'default', 'Colaborador', 1, 95),
(11, 'David', 'Pedraza', 'M', 'davipep@gmail.com', 'Cedula de Ciudadania', '12345', 'Estudiante', 'default', 'Colaborador', 1, 90),
(12, 'Juan', 'Cortez', 'M', 'juancha@hotmail.com', 'Cedula de Ciudadania', '32789992', 'Estudiante', 'default', 'Colaborador', 1, 96),
(13, 'Miguel', 'Herrera', 'M', 'miguelh@gmail.com', 'Cedula de Ciudadania', '290290290', 'Administrativo', 'default', 'Colaborador', 1, 72),
(14, 'Juana', 'Carrillo', 'F', 'juanua@mail.com', 'Cedula de Ciudadania', '9080890', 'Profesor', 'c893bad68927b457dbed39460e6afd62', 'Responsable', 1, 31),
(15, 'Rafael', 'Ferrer', 'M', 'datosfacil@gmail.com', 'Tarjeta de Identidad', '19029818', 'Profesor', '08a5d905629acfbe8f52b2bed107c6fd', 'Responsable', 1, 31),
(16, 'Dane', 'Wilson', 'M', 'willson@gmail.com', 'Cedula de Ciudadania', '67829761', 'Administrativo', '5fd40a797078bc13bba5c6dcd2259b3b', 'Responsable', 1, 31),
(17, 'Dora Stella', 'Melo Hurtado', 'F', 'ds.meloh@javeriana.edu.co', 'Cedula de Ciudadania', '51825080', 'Profesor', '0ff7ed3c48221f123d477704040258a9', 'Responsable', 1, 31),
(18, 'Paola', 'PachÃ³n', 'F', 'ppachon@javeriana.edu.co', 'Cedula de Ciudadania', '1022972086', 'Estudiante', 'default', 'Colaborador', 1, 79),
(19, 'jose ', 'perez', 'M', 'j.p@gmail.com', 'Cedula de Ciudadania', '09876', 'Estudiante', 'default', 'Colaborador', 1, 69),
(20, 'jose ', 'perez', 'M', 'j.p@gmail.com', 'Cedula de Ciudadania', '987654', 'Estudiante', 'default', 'Colaborador', 1, 69),
(21, 'fer', 'ochoa', 'M', 'ocho@gmail.com', 'Cedula de Ciudadania', '234567', 'Estudiante', 'default', 'Colaborador', 1, 75),
(22, 'iniuniuniu', 'iuniuniuniun', 'M', 'omiomiom', 'Cedula de Ciudadania', '888', 'Estudiante', 'default', 'Colaborador', 1, 69),
(47, 'Juan', 'Porto', 'M', 'daniel.abela88@gmail.com', 'Cedula de Ciudadania', '8768896', 'Administrativo', 'default', 'Colaborador', 1, 69);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesiones`
--

CREATE TABLE IF NOT EXISTS `sesiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(1200) DEFAULT NULL,
  `lugar` varchar(150) NOT NULL,
  `fechaHora` datetime DEFAULT NULL,
  `duracion` varchar(50) NOT NULL,
  `id_actividad` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IXFK_Sesiones_Actividades` (`id_actividad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=150 ;

--
-- Volcado de datos para la tabla `sesiones`
--

INSERT INTO `sesiones` (`id`, `nombre`, `descripcion`, `lugar`, `fechaHora`, `duracion`, `id_actividad`) VALUES
(32, 'asdasd', 'asdasca', 'Colegio', '2017-10-12 09:00:00', '01:00', 14),
(33, 'ascasc', 'asca', 'Colegio', '2017-10-12 12:00:00', '01:00', 14),
(74, 'Inicio', 'Inicio de la actividad', 'Colegio', '2017-10-12 09:00:00', '01:00', 40),
(75, 'Lavado', 'Lavado de manos', 'Colegio', '2017-10-12 10:00:00', '01:00', 40),
(76, 'Encuentro', 'Encuentro con los padres de familia', 'Colegio', '2017-10-12 00:00:00', '01:00', 41),
(77, 'Charla informativa', 'Se inicia la charla informativa', 'Colegio', '2017-10-12 12:00:00', '02:30', 41),
(78, 'Inicio', 'inicio', 'Colegio', '2017-10-12 09:00:00', '01:00', 42),
(79, 'Final', 'Final', 'Colegio', '2017-10-12 10:00:00', '02:00', 42),
(81, 'Estilos de Vida', 'Taller estilos de vida con niÃ±os del colegio sabio caldas', 'Colegio', '2017-11-02 09:00:00', '11:00', 44),
(90, 'seccion 1', 'ninguna', 'Colegio', '2017-10-27 06:30:00', '01:00', 49),
(91, 'h7ujiko', 'tgyhuj', 'Colegio', '2017-10-28 09:00:00', '01:00', 49),
(119, 'estillos de vida', 'niÃ±os del colegio sabio caldas', 'Colegio', '2017-11-08 08:23:00', ' 08:23', 64),
(120, 'qweqw', 'qweqwe', 'Colegio', '2017-11-10 11:17:00', ' 11:17', 64),
(147, 'Calentamiento', 'Jugadores', 'Estadio', '2017-12-02 09:30:00', '01:00', 91);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidadesacademicas`
--

CREATE TABLE IF NOT EXISTS `unidadesacademicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) CHARACTER SET utf8 NOT NULL,
  `tipo` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `id_unidad_academica` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_UnidadesAcademicas_UnidadesAcademicas` (`id_unidad_academica`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=98 ;

--
-- Volcado de datos para la tabla `unidadesacademicas`
--

INSERT INTO `unidadesacademicas` (`id`, `nombre`, `tipo`, `id_unidad_academica`) VALUES
(1, 'Arquitectura y Diseno', 'Facultad', NULL),
(2, 'Artes', 'Facultad', NULL),
(3, 'Estetica', 'Facultad', NULL),
(4, 'Ciencias', 'Facultad', NULL),
(5, 'Ciencias economicas y Administrativas', 'Facultad', NULL),
(6, 'Ciencias Politicas y Relaciones Internacionales', 'Facultad', NULL),
(7, 'Ciencias Sociales', 'Facultad', NULL),
(8, 'Comunicacion y Lenguaje', 'Facultad', NULL),
(9, 'Derecho Canonico', 'Facultad', NULL),
(10, 'Educacion', 'Facultad', NULL),
(11, 'Enfermeria', 'Facultad', NULL),
(12, 'Estudios Ambientales y Rurales', 'Facultad', NULL),
(13, 'Filosofia', 'Facultad', NULL),
(14, 'Ingenieria', 'Facultad', NULL),
(15, 'Medicina', 'Facultad', NULL),
(16, 'Odontologia', 'Facultad', NULL),
(17, 'Psicologia', 'Facultad', NULL),
(18, 'Teologia', 'Facultad', NULL),
(19, 'Arquitectura', 'Departamento', NULL),
(20, 'Diseno ', 'Departamento', NULL),
(21, 'Estetica', 'Departamento', NULL),
(22, 'Artes Escenicas', 'Departamento', NULL),
(23, 'Artes Visuales', 'Departamento', NULL),
(24, 'Musica', 'Departamento', NULL),
(25, 'Biologia', 'Departamento', NULL),
(26, 'Fisica', 'Departamento', NULL),
(27, 'Matematicas', 'Departamento', NULL),
(28, 'Microbiologia', 'Departamento', NULL),
(29, 'Nutricion y Bioquimica', 'Departamento', NULL),
(30, 'Quimica', 'Departamento', NULL),
(31, 'Administracion', 'Departamento', NULL),
(32, 'Ciencias Contables', 'Departamento', NULL),
(33, 'Economia', 'Departamento', NULL),
(34, 'Derecho economico', 'Departamento', NULL),
(35, 'Derecho Laboral', 'Departamento', NULL),
(36, 'Derecho Penal', 'Departamento', NULL),
(37, 'Derecho Privado', 'Departamento', NULL),
(38, 'Derecho Procesal', 'Departamento', NULL),
(39, 'Derecho Publico', 'Departamento', NULL),
(40, 'Filosofia e Historia del Derecho', 'Departamento', NULL),
(41, 'Ciencia Politica', 'Departamento', NULL),
(42, 'Relaciones Internacionales', 'Departamento', NULL),
(43, 'Antropologia', 'Departamento', NULL),
(44, 'Estudios Culturales', 'Departamento', NULL),
(45, 'Historia', 'Departamento', NULL),
(46, 'Literatura', 'Departamento', NULL),
(47, 'Sociologia', 'Departamento', NULL),
(48, 'Ciencia de la Informacion', 'Departamento', NULL),
(49, 'Comunicacion', 'Departamento', NULL),
(50, 'Lenguas', 'Departamento', NULL),
(51, 'Formacion', 'Departamento', NULL),
(52, 'Enfermeria en salud Colectiva', 'Departamento', NULL),
(53, 'Enfermeria Clinica', 'Departamento', NULL),
(54, 'Desarrollo Rural y Regional', 'Departamento', NULL),
(55, 'Ecologia y Territorio', 'Departamento', NULL),
(56, 'Filosofia', 'Departamento', NULL),
(57, 'Civil', 'Departamento', NULL),
(58, 'Arquitectura', 'Programa', NULL),
(59, 'Diseno Industrial', 'Programa', NULL),
(60, 'Esp. Diseno y Gerencia de Producto', 'Programa', NULL),
(61, 'Artes Escenicas', 'Programa', NULL),
(62, 'Artes Visuales', 'Programa', NULL),
(63, 'Estudios Musicales', 'Programa', NULL),
(64, 'Biologia', 'Programa', NULL),
(65, 'Bacteriologia', 'Programa', NULL),
(66, 'Nutricion y Dietetica', 'Programa', NULL),
(67, 'Microbiologia Industrial', 'Programa', NULL),
(68, 'Matematicas', 'Programa', NULL),
(69, 'Administracion de Empresas', 'Programa', NULL),
(70, 'Contaduria Publica', 'Programa', NULL),
(71, 'Economia', 'Programa', NULL),
(72, 'Derecho', 'Programa', NULL),
(73, 'Ciencia Politica', 'Programa', NULL),
(74, 'Relaciones Internacionales', 'Programa', NULL),
(75, 'Antropologia', 'Programa', NULL),
(76, 'Historia', 'Programa', NULL),
(77, 'Estudios Literarios', 'Programa', NULL),
(78, 'Sociologia', 'Programa', NULL),
(79, 'Ciencia de la Informacion - Bibliotecologia', 'Programa', NULL),
(80, 'Comunicacion Social', 'Programa', NULL),
(81, 'Lic. en Lenguas', 'Programa', NULL),
(82, 'Lic. En Filosofia', 'Programa', NULL),
(83, 'Lic. En educacion Infantil', 'Programa', NULL),
(84, 'Enfermeria', 'Programa', NULL),
(85, 'Ecologia', 'Programa', NULL),
(86, 'Filosofia', 'Programa', NULL),
(87, 'Ingenieria Civil', 'Programa', NULL),
(88, 'Ingenieria Electronica', 'Programa', NULL),
(89, 'Ingenieria Industrial', 'Programa', NULL),
(90, 'Ingenieria de Sistemas', 'Programa', NULL),
(91, 'Medicina', 'Programa', NULL),
(92, 'Esp. En Geriatria', 'Programa', NULL),
(93, 'Esp. En Pediatria', 'Programa', NULL),
(94, 'Odontologi', 'Programa', NULL),
(95, 'Psicologia', 'Programa', NULL),
(96, 'Teologia', 'Programa', NULL),
(97, 'Lic. En Teologia', 'Programa', NULL);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `activiadades_beneficiarios`
--
ALTER TABLE `activiadades_beneficiarios`
  ADD CONSTRAINT `FK_Activiadades_Beneficiarios_Actividades` FOREIGN KEY (`id_actividades`) REFERENCES `actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Activiadades_Beneficiarios_Beneficiarios` FOREIGN KEY (`id_beneficiarios`) REFERENCES `beneficiarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD CONSTRAINT `FK_Actividades_Actividades` FOREIGN KEY (`id_actividad`) REFERENCES `actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Actividades_Colaboradores` FOREIGN KEY (`id_colaborador`) REFERENCES `colaboradores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Actividades_Colaboradores_Creador` FOREIGN KEY (`id_creador`) REFERENCES `colaboradores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `actividades_colaboradores`
--
ALTER TABLE `actividades_colaboradores`
  ADD CONSTRAINT `FK_Actividades_Beneficiarios_Actividades` FOREIGN KEY (`id_actividades`) REFERENCES `actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Actividades_Beneficiarios_Colaboradores` FOREIGN KEY (`id_colaboradores`) REFERENCES `colaboradores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `colaboradores`
--
ALTER TABLE `colaboradores`
  ADD CONSTRAINT `FK_Colaboradores_UnidadesAcademicas` FOREIGN KEY (`id_unidad_academica`) REFERENCES `unidadesacademicas` (`id`);

--
-- Filtros para la tabla `sesiones`
--
ALTER TABLE `sesiones`
  ADD CONSTRAINT `FK_Sesiones_Actividades` FOREIGN KEY (`id_actividad`) REFERENCES `actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `unidadesacademicas`
--
ALTER TABLE `unidadesacademicas`
  ADD CONSTRAINT `FK_UnidadesAcademicas_UnidadesAcademicas` FOREIGN KEY (`id_unidad_academica`) REFERENCES `unidadesacademicas` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
