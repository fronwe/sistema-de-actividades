-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-08-2017 a las 06:57:10
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `psu`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activiadades_beneficiarios`
--

CREATE TABLE `activiadades_beneficiarios` (
  `id_actividades` int(11) NOT NULL,
  `id_beneficiarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades`
--

CREATE TABLE `actividades` (
  `id` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `objetivo` varchar(1200) NOT NULL,
  `resultado` varchar(1200) DEFAULT NULL,
  `fecha` date NOT NULL,
  `estado` varchar(50) NOT NULL,
  `volumen` int(11) NOT NULL,
  `temporalidad` varchar(50) DEFAULT NULL,
  `convocatoria` varchar(50) NOT NULL,
  `id_actividad` int(11) DEFAULT NULL,
  `id_colaborador` int(11) DEFAULT NULL,
  `id_creador` int(11) DEFAULT NULL,
  `tipo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `actividades`
--

INSERT INTO `actividades` (`id`, `nombre`, `objetivo`, `resultado`, `fecha`, `estado`, `volumen`, `temporalidad`, `convocatoria`, `id_actividad`, `id_colaborador`, `id_creador`, `tipo`) VALUES
(1, 'Inicio de actividades', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore porro sequi, et neque voluptatem doloremque tenetur ea non. Eos, quibusdam!', 'NULL', '2017-05-31', 'Pendiente', 50, '00:30', 'Masiva', NULL, 14, 1, 'Capacitacion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades_colaboradores`
--

CREATE TABLE `actividades_colaboradores` (
  `id_actividades` int(11) NOT NULL,
  `id_colaboradores` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `actividades_colaboradores`
--

INSERT INTO `actividades_colaboradores` (`id_actividades`, `id_colaboradores`) VALUES
(1, 17),
(1, 28),
(1, 31);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `beneficiarios`
--

CREATE TABLE `beneficiarios` (
  `id` int(11) NOT NULL,
  `tipo_identificacion` varchar(50) DEFAULT NULL,
  `identificacion` varchar(50) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `beneficiarios`
--

INSERT INTO `beneficiarios` (`id`, `tipo_identificacion`, `identificacion`, `nombre`, `fecha_nacimiento`) VALUES
(1, 'Cedula de Ciudadania', '7890187', 'Luis Castillo', '2017-04-08'),
(2, 'Cedula de Ciudadania', '234123', 'Juan Camilo FLorez', '1962-09-11'),
(3, 'Cedula de Ciudadania', '765345', 'David Suarez', '1965-03-22'),
(4, 'Cedula de Ciudadania', '3567678', 'Andrea Camacho', '1970-02-02'),
(5, 'Cedula de Ciudadania', '7879554', 'Maria Manrrique', '1972-06-14'),
(6, 'Cedula de Ciudadania', '2345245', 'Diego Sanchez', '1960-08-25'),
(7, 'Cedula de Ciudadania', '876667', 'Fabian Uribe', '1945-10-19'),
(8, 'Cedula de Ciudadania', '1616745', 'Iad Sevilla', '1923-09-23'),
(9, 'Cedula de Ciudadania', '1234654', 'Sergio Plazas', '1923-11-09'),
(10, 'Cedula de Ciudadania', '987635', 'Carolina Arteaga', '1963-04-14'),
(11, 'Cedula de Ciudadania', '5234343', 'CataLina Abella', '1990-01-22'),
(12, 'Cedula de Ciudadania', '87646', 'Cesar Lara', '1956-06-06'),
(13, 'Cedula de Ciudadania', '9987567', 'David Acosta', '1934-04-10'),
(14, 'Cedula de Ciudadania', '16545636', 'Maria Fernanda', '1923-02-12'),
(15, 'Cedula de Ciudadania', '385617', 'Luna Gonzales', '1945-05-13'),
(16, 'Cedula de Ciudadania', '4567810', 'Luis Enriqeu Castillo', NULL),
(17, 'Cedula de Ciudadania', '678910199', 'Tatiana Preciado', '2017-04-13'),
(18, 'Cedula de Ciudadania', '678920', 'Moices Aaguabara', NULL),
(27, 'Cedula de Ciudadania', '51664989', 'Martha Rojas Diaz', NULL),
(28, 'Cedula de Ciudadania', '19398214', 'Carlos Abella', NULL),
(29, 'Cedula de Ciudadania', '000000', 'Jhon Camacho', NULL),
(30, 'Cedula de Ciudadania', '1902099101', 'Camilo Bernal', NULL),
(31, 'Pasaporte', '678988879', 'Juan Camilo Abella', NULL),
(32, 'Cedula de Ciudadania', '90000090', 'Camilo Bernal', NULL),
(33, 'Cedula de Ciudadania', '1000090', 'Jesus Casas', NULL),
(34, 'Cedula de Ciudadania', '6787', 'mono', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colaboradores`
--

CREATE TABLE `colaboradores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `tipo_identificacion` varchar(50) NOT NULL,
  `identificacion` varchar(50) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `clave` varchar(60) NOT NULL,
  `permiso` varchar(20) NOT NULL,
  `id_unidad_academica` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `colaboradores`
--

INSERT INTO `colaboradores` (`id`, `nombre`, `email`, `tipo_identificacion`, `identificacion`, `tipo`, `clave`, `permiso`, `id_unidad_academica`) VALUES
(1, 'Sergio Abella', 'sergioabella63@gmail.com', 'Cedula de Ciudadania', '1019086911', 'Administrativo', 'tomcax1', 'administrador', 4),
(11, 'Andres Castro', 'andrescastro_99@hotmail.com', 'Cedula de Ciudadania', '190199209', 'Estudiante', 'andres1234', 'usuario', 4),
(12, 'Camilo Cifuentes', 'camilocif80@gmail.com', 'Tarjeta de Identidad', '102901990', 'Administrativo', 'camilo1234', 'usuario', 1),
(13, 'David Pedraza', 'davipep@hotmail.com', 'Tarjeta de Identidad', '90892819', 'Estudiante', 'davip1234', 'usuario', 4),
(14, 'Maida Moreno', 'maida90@hotmail.com', 'Cedula de Ciudadania', '902892', 'Administrativo', 'maida1234', 'administrador', 4),
(15, 'Efrain Sanches', 'efraincoord@hotmail.com', 'Cedula de Ciudadania', '90289188', 'Administrativo', 'efrain1234', 'usuario', 1),
(16, 'Miguel Torres', 'miguelback@hotmail.com', 'Cedula de Ciudadania', '1902919', 'Estudiante', 'miguel1234', 'usuario', 1),
(17, 'Andre Bojart', 'andrev@gmail.com', 'Cedula de Extranjero', '90189288', 'Colaborador', 'andre1234', 'administrador', 4),
(18, 'Carlina Herrera', 'caro34@gmail.com', 'Cedula de Ciudadania', '27892871', 'Estudiante', 'carolina1234', 'usuario', 1),
(19, 'Andrea Palacios', 'andre90@hotmail.com', 'Cedula de Ciudadania', '2892001', 'Estudiante', 'andrea1234', 'usuario', 1),
(20, 'Valeria Delgado', 'valeria@gmail.com', 'Cedula de Ciudadania', '9087879', 'Estudiante', 'valeria1234', 'usuario', 1),
(21, 'Camila Cantona', 'camila20@hotmail.com', 'Cedula de Ciudadania', '8903890', 'Administrativo', 'camila1234', 'administrador', 1),
(22, 'Simon Arciniegas', 'simon@gmail.com', 'Cedula de Ciudadania', '789319098', 'Estudiante', 'simon1234', 'usuario', 6),
(23, 'Felipe Madero', 'felipemad@gmail.com', 'Cedula de Ciudadania', '90839181', 'Estudiante', 'madero1234', 'usuario', 6),
(24, 'Mateo Diaz', 'mateod@hotmail.com', 'Cedula de Extranjero', '1201920019', 'Estudiante', 'mateo1234', 'administrador', 6),
(25, 'Daniella Herrera', 'daniellaherrera28@hotmail.com', 'Cedula de Ciudadania', '89038299', 'Administrativo', 'daniella1234', 'administrador', 6),
(26, 'Santiago Vesga', 'santiagovesg@gmail.com', 'Tarjeta de Identidad', '10291483', 'Administrativo', 'santiago1234', 'usuario', 6),
(27, 'Nicole Arias', 'nicolari@hotmail.com', 'Cedula de Extranjero', '1289128', 'Colaborador', 'nicol1234', 'usuario', 6),
(28, 'Moises Abuabara', 'moiso@hotmail.com', 'Cedula de Ciudadania', '47892820', 'Colaborador', 'mioses1234', 'usuario', 6),
(29, 'Miguel Basques', 'miguel@hotmail.cm', 'Cedula de Ciudadania', '89438920', 'Estudiante', 'miguel1234', 'administrador', 8),
(30, 'Gabriel Mendoza', 'gabo@gmail.com', 'Cedula de Ciudadania', '120199281', 'Estudiante', 'gabo1234', 'usuario', 8),
(31, 'Miguel Calvo', 'miguelcalv80@hotmail.com', 'Cedula de Ciudadania', '3289302019', 'Estudiante', '1234', 'usuario', 8),
(32, 'Jhon Camacho', 'jjcamachosanchez@gmail.com', 'Cedula de Ciudadania', '100190090', 'Administrativo', 'jhoncamacho1234', 'usuario', 1),
(33, 'Paula Martinez', 'paula90@gmail.com', 'Cedula de Ciudadania', '198200190', 'Estudiante', 'paula1234', 'usuario', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesiones`
--

CREATE TABLE `sesiones` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(1200) DEFAULT NULL,
  `fechaHora` datetime DEFAULT NULL,
  `id_actividad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sesiones`
--

INSERT INTO `sesiones` (`id`, `nombre`, `descripcion`, `fechaHora`, `id_actividad`) VALUES
(1, 'Llegada de colaboradores', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore porro sequi, et neque', '2017-05-31 09:30:00', 1),
(2, 'Retroalimentacion', 'Sequi, et neque voluptatem doloremque tenetur ea non. Eos, quibusdam!', '2017-05-31 10:00:00', 1),
(3, 'Finalizacion', 'Aipisicing elit. Tempore porro seq', '2017-05-31 11:15:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidadesacademicas`
--

CREATE TABLE `unidadesacademicas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `id_unidad_academica` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `unidadesacademicas`
--

INSERT INTO `unidadesacademicas` (`id`, `nombre`, `tipo`, `id_unidad_academica`) VALUES
(1, 'Comunicacion', 'Departamento', NULL),
(2, 'Ingenieria', 'Programa', NULL),
(3, 'Medicina', 'Programa', NULL),
(4, 'Ingenieria de Sistemas', 'Departamento', 2),
(5, 'Ingenieria Industrial', 'Departamento', 2),
(6, 'Derecho', 'Programa', NULL),
(7, 'Enfermeria', 'Departamento', 3),
(8, 'Diseno Industrial', 'Programa', NULL),
(9, 'Musica', 'Programa', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activiadades_beneficiarios`
--
ALTER TABLE `activiadades_beneficiarios`
  ADD PRIMARY KEY (`id_actividades`,`id_beneficiarios`),
  ADD KEY `IXFK_Activiadades_Beneficiarios_Actividades` (`id_actividades`),
  ADD KEY `IXFK_Activiadades_Beneficiarios_Beneficiarios` (`id_beneficiarios`);

--
-- Indices de la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_Actividades_Actividades` (`id_actividad`),
  ADD KEY `IXFK_Actividades_Colaboradores` (`id_colaborador`),
  ADD KEY `IXFK_Actividades_Colaboradores_Creador` (`id_creador`);

--
-- Indices de la tabla `actividades_colaboradores`
--
ALTER TABLE `actividades_colaboradores`
  ADD PRIMARY KEY (`id_actividades`,`id_colaboradores`),
  ADD KEY `IXFK_Actividades_Beneficiarios_Actividades` (`id_actividades`),
  ADD KEY `IXFK_Actividades_Beneficiarios_Colaboradores` (`id_colaboradores`);

--
-- Indices de la tabla `beneficiarios`
--
ALTER TABLE `beneficiarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `colaboradores`
--
ALTER TABLE `colaboradores`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `identificacion` (`identificacion`),
  ADD KEY `IXFK_Colaboradores_UnidadesAcademicas` (`id_unidad_academica`);

--
-- Indices de la tabla `sesiones`
--
ALTER TABLE `sesiones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IXFK_Sesiones_Actividades` (`id_actividad`);

--
-- Indices de la tabla `unidadesacademicas`
--
ALTER TABLE `unidadesacademicas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_UnidadesAcademicas_UnidadesAcademicas` (`id_unidad_academica`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividades`
--
ALTER TABLE `actividades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `beneficiarios`
--
ALTER TABLE `beneficiarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `colaboradores`
--
ALTER TABLE `colaboradores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `sesiones`
--
ALTER TABLE `sesiones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `unidadesacademicas`
--
ALTER TABLE `unidadesacademicas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `activiadades_beneficiarios`
--
ALTER TABLE `activiadades_beneficiarios`
  ADD CONSTRAINT `FK_Activiadades_Beneficiarios_Actividades` FOREIGN KEY (`id_actividades`) REFERENCES `actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Activiadades_Beneficiarios_Beneficiarios` FOREIGN KEY (`id_beneficiarios`) REFERENCES `beneficiarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD CONSTRAINT `FK_Actividades_Actividades` FOREIGN KEY (`id_actividad`) REFERENCES `actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Actividades_Colaboradores` FOREIGN KEY (`id_colaborador`) REFERENCES `colaboradores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Actividades_Colaboradores_Creador` FOREIGN KEY (`id_creador`) REFERENCES `colaboradores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `actividades_colaboradores`
--
ALTER TABLE `actividades_colaboradores`
  ADD CONSTRAINT `FK_Actividades_Beneficiarios_Actividades` FOREIGN KEY (`id_actividades`) REFERENCES `actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Actividades_Beneficiarios_Colaboradores` FOREIGN KEY (`id_colaboradores`) REFERENCES `colaboradores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `colaboradores`
--
ALTER TABLE `colaboradores`
  ADD CONSTRAINT `FK_Colaboradores_UnidadesAcademicas` FOREIGN KEY (`id_unidad_academica`) REFERENCES `unidadesacademicas` (`id`);

--
-- Filtros para la tabla `sesiones`
--
ALTER TABLE `sesiones`
  ADD CONSTRAINT `FK_Sesiones_Actividades` FOREIGN KEY (`id_actividad`) REFERENCES `actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `unidadesacademicas`
--
ALTER TABLE `unidadesacademicas`
  ADD CONSTRAINT `FK_UnidadesAcademicas_UnidadesAcademicas` FOREIGN KEY (`id_unidad_academica`) REFERENCES `unidadesacademicas` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
